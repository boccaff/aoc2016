#!/usr/bin/env python
import argparse
import matplotlib.pyplot as plt
import numpy as np


def parseline(line):
    x = line.split(":")
    day = x[0]
    x = x[1].split(", ")
    runtime = x[1]
    return (day, runtime)


def parsetime(timestr):
    if timestr.endswith("µs"):
        return 0.001 * float(timestr[:-2])
    elif timestr.endswith("ms"):
        return float(timestr[:-2])
    else:
        return 1000 * float(timestr[:-1])


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-f", "--filename", dest="filename", type=str)
    args = parser.parse_args()

    with open(args.filename, "r") as f:
        data = [
            parseline(line.strip()) for line in f.readlines() if line.startswith("D")
        ]
    data = [(*line, parsetime(line[1])) for line in data]

    data = sorted(data, key=lambda x: -x[2])

    days = np.array([x[0] for x in data])
    labels = np.array([x[1] for x in data])
    duration = np.array([x[2] for x in data])
    absacc = np.cumsum(duration)
    accduration = absacc / np.sum(duration)
    dummy_x = np.arange(len(data))

    for i, l in enumerate(data):
        print(f"{l[0]}\t{l[1]}\t{l[2]:.2f}\t{absacc[i]:.2f}\t{accduration[i]:.2%}")

    fig, ax = plt.subplots(1, 1, figsize=(16, 9))
    ax.bar(days[:7], duration[:7])
    ax2 = ax.twinx()
    ax2.plot(dummy_x[:7], accduration[:7], color="red")
    plt.savefig("runtime.png")


if __name__ == "__main__":
    main()
