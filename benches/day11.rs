use criterion::{criterion_group, criterion_main, Criterion};
pub fn day11(c: &mut Criterion) {
    // begin setup
    use aoc2016::days::day11::{parse_input, solution};
    c.bench_function("parse_input", |b| {
        b.iter(|| {
            parse_input("./input/11.txt");
        })
    });
    let input = parse_input("input/11.txt");
    c.bench_function("solution", |b| {
        b.iter(|| {
            solution(&input);
        })
    });
    let mut input = parse_input("input/11.txt");
    input.1[5] = 1;
    input.1[6] = 1;
    input.0[5] = 1;
    input.0[6] = 1;
    c.bench_function("solution", |b| {
        b.iter(|| {
            solution(&input);
        })
    });
}
criterion_group!(benches, day11);
criterion_main!(benches);
