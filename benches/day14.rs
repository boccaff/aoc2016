use criterion::{criterion_group, criterion_main, Criterion};
pub fn day14(c: &mut Criterion) {
    // begin setup
    use aoc2016::days::day14::{generate_keys, parse_input};
    c.bench_function("parse_input", |b| {
        b.iter(|| {
            parse_input("./input/14.txt");
        })
    });
    // let input = parse_input("input/14.txt");
    // c.bench_function("solution", |b| {
    //     b.iter(|| {
    //         generate_keys(&input, 1);
    //     })
    // });
    let input = parse_input("input/14.txt");
    c.bench_function("solution", |b| {
        b.iter(|| {
            generate_keys(&input, 2017);
        })
    });
}
criterion_group!(benches, day14);
criterion_main!(benches);
