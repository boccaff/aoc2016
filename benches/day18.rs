use criterion::{criterion_group, criterion_main, Criterion};
pub fn day18(c: &mut Criterion) {
    // begin setup
    use aoc2016::days::day18::{parse_input, part1};
    c.bench_function("parse_input", |b| {
        b.iter(|| {
            parse_input("./input/18.txt");
        })
    });
    let input = parse_input("input/18.txt");
    c.bench_function("solution", |b| {
        b.iter(|| {
            part1(&input, 40);
        })
    });

    c.bench_function("solution", |b| {
        b.iter(|| {
            part1(&input, 400_000);
        })
    });
}
criterion_group!(benches, day18);
criterion_main!(benches);
