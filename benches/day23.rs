use criterion::{criterion_group, criterion_main, Criterion};
pub fn day23(c: &mut Criterion) {
    // begin setup
    use aoc2016::days::day23::solve;
    use aoc2016::parse_assembunny_program;
    c.bench_function("parse_input", |b| {
        b.iter(|| {
            parse_assembunny_program("./input/23.txt");
        })
    });
    c.bench_function("solution", |b| {
        b.iter(|| {
            solve();
        })
    });
}
criterion_group!(benches, day23);
criterion_main!(benches);
