use criterion::{criterion_group, criterion_main, Criterion};
pub fn day05(c: &mut Criterion) {
    // begin setup
    use aoc2016::days::day05::{parse_input, solution};
    c.bench_function("parse_input", |b| {
        b.iter(|| {
            parse_input("./input/05.txt");
        })
    });
    let input = parse_input("input/05.txt");
    c.bench_function("solution", |b| {
        b.iter(|| {
            solution(&input);
        })
    });
}
criterion_group!(benches, day05);
criterion_main!(benches);
