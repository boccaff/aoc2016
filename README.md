# Christmas on July

Sometimes, you just want to code something for fun, and on June 20, 2024, I've reached for the 2016 Advent of Code (AoC). Since I started AoC in 2021, it's been nice to go back to the first years, and see how it was in the begining. I've gone through 2015 in the months leading up to Dec 2023 to brush up Rust, which is the language I've used for 2021 and 2022. As I don't use it in my day to day, it was a nice way to get up to speed for the advent calendar. This time, it was just for fun. After doing it on and off for a few days, I was hooked by the more interesting problems at the end of the calendar. I had only completed the first 10 days between June 20 and July 10, but completed the remainder 15 days before July 31. If you don't know AoC, it is a lot more work, as problems get exponentially harder at the end. You can see the decay in the [number of people](https://adventofcode.com/2016/stats) that can complete each day. The rest of this text will contain spoilers for part 2 of the problems (when doing AoC, you can only see the description for the first part, and you unlock the part 2 description after completing the first one). After completing all days, I've spend some time [optimizing](PERF.md) the time required to run all days, being able to move from 9 seconds to close to 900ms. At this point, most of the time is spent calculating md5 hashes for days 05 and 14. All days have at least a test for part1, with 35 tests implemented. Some redundancy exists, but the libraries used are:

``` 
base16ct = {version = "0.2.0", features = ["alloc"]}
hashbrown = "0.14.3"
itertools = "0.13.0"
md-5 = "0.10.6"
nalgebra = "0.32.6"
pathfinding = "4.10.0"
rayon = "1.7"
scan_fmt = "0.2.6"
rust-crypto = "0.2.36"
threadpool = "1.8.1"
``` 

and running in `--release` results in:

```
Day 01: 360.89µs
Day 02: 67.96µs
Day 03: 767.82µs
Day 04: 1.93ms
Day 05: 522.94ms
Day 06: 130.90µs
Day 07: 17.27ms
Day 08: 156.00µs
Day 09: 503.98µs
Day 10: 390.49µs
Day 11: 104.79ms
Day 12: 81.53ms
Day 13: 183.35µs
Day 14: 868.01ms
Day 15: 109.08ms
Day 16: 109.17ms
Day 17: 203.28ms
Day 18: 388.00ms
Day 19: 8.02µs
Day 20: 1.42ms
Day 21: 12.20ms
Day 22: 262.60ms
Day 23: 122.97µs
Day 24: 11.28ms
Day 25: 11.52µs
Total 868.61ms
```

## Problems

While 2015 had some "pilot episode" feel, 2016 already felt like 2021-2023. I felt it was an easier year, but I can't discount that this is the fifth event I've completed. Hashing and square movements in grids were a recurring pattern here, with day XX being a nice merge between those two themes along with a potential graph transversal problem.

 - **Day01**: Part 1 (p1) is just following instructions for walking in a grid, with part 2 (p2) asking for the location that you visit twice.
 - **Day02**: In p1, you used sequential directions (like UDLL for Up Down Left Left) on a grid to find the numbers in a keypad that would form a password. In p2, the grid was no longer a standard numeric keyboard and had a rhombus shape.
 - **Day03**: Lists of 3 numbers that you have to check if they can or cannot be a triangle. In the second part, the numbers should be checked between lines, and not in the same line.
 - **Day04**: First hashing problem, where you implement a simple hashing algorithm and check if some hashes match. For the second part, you need to search something based on the room name.
 - **Day05**: Sequential applications of MD5 to build a movie like "password breaking" scene where each character is figured out. I was very close to trying to implement MD5, but decided to go for a library (Maybe in another year). In the second part, the instructions become more complicated, and you use the hash to find the position to add chars.
 - **Day06**: Straightforward count of chars in positions. I've just counted the occurrence of letters across lines for each position, and later sorted to find the most frequent and least frequent chars.
 - **Day07**: This was an odd one, as you had strings and had to check some conditions on it based on substrings contained or not inside square brackets. I don't know if I dislike it because handling strings and arbitrary conditions feels to much like one of the worst parts of being a data scientist, or if I am missing something here about a general pattern matching. I can see that this may be similar to build a expression parser, but I am just glad that I've finished it. 
 - **Day08**: This is a problem that show up in different flavors accross years, and a [harder version](https://adventofcode.com/2021/day/13) got me in 2021. You have some representation in a grid, and you have to reshape it successively until some ascii printed will form a pattern that you can type to solve the puzzle.
 - **Day09**: A very nice problem with a compression algorithm. Here you have an AoC pattern where you are asked to do some processing and calculate something on it, but the second part is intractable (or maybe would take a LOT of time and/or RAM to do) and you can find your way out a way to do just the calculating that is asked instead of performing the operation. I guess that for people that go for the leaderboard, there is a gamble here. If P2 just ask again for the calculation, you wasted your time implementing the transformation. But if p1 was just a quick test to see if your transformation implementation is correct, you would need to implement the transformation later.
 - **Day10**: This one reminds me of [2022, day11](https://adventofcode.com/2022/day/11). You have a set of instructions about propagating values, but they are presented in ordering that maybe not sufficient to evaluate when you see them at first. This amounts to building a expression and calculating it when all the values are available, and doing it iteratively and incrementally. Probably another crack in CS knowledge coming from my non-CS background.
 - **Day11**: This is also a very nice problem, where you have to move items across different floors, but not all items can be moved or be left alone safely. Imagine the wolf, goat and cabbage across the river riddle, but more complex. P2 just increase the number of items and can make some inneficient solutions explode. After finishing up the whole year, it was my slowest day.
 - **Day12**: An intcode problem! Here you implement a tiny virtual machine, using a list of instructions in `assembunny` to perform calculations. After finishing some problems like this, I am always left with an idea to really look into how machine code, assembly, and etc. It was nice to see this reused later, and it gives a hint about what to expect when I finnaly get to do [2019](https://www.reddit.com/r/adventofcode/comments/kkhnpk/whats_up_with_intcode_2019/).
 - **Day13**: Shortest path in a maze built with a function to determine if a space is empty or not. Second part asks for all different locations reachable with a certain number of steps.
 - **Day14**: Successively generate hashes and for a hash candidate, checks for patterns in the hashes following this candidate to see if it is a key. It is largely cpu bound by MD5 calculation, and unless we can leverage another way to calculate the hashes, there is no speeding up this one.
 - **Day15**: Find how long to wait so that you can align multiple oscillating plates and a ball fall through them. This is brute forced, and probably can be improved with some GCD logic.
 - **Day16**: Recursive string processing for a `dragon curve` inspired string.
 - **Day17**: Maze where moving is conditioned by solving md5 hashes based on the state. Oddly fast.
 - **Day18**: Cellular automata simulation. A Vec solution was used but maybe a bitmask solution could be faster.
 It is [Rule 90](https://mathworld.wolfram.com/Rule90.html) from the elementary cellular automata cataloged by Wolfram, and I'd guess that its fractal properties causes the input to not recur over the iterations for P2. The large pattern will require a lib to rebuild this with a bitmask.
 - **Day19**: [Josephus Problem](https://www.youtube.com/watch?v=uCsD3ZGzMgE)! What a nice way to learn about a problem, and what a nice video from Numberphile. It was nice to mix up some pen and paper along with coding.
 - **Day20**: Interval operations, maybe the first one? I think that almost every year have something like this.
 - **Day21**: Mutate string based on a set of instructions. Part 2 was brute forced and maybe can leverage some batched parallel evaluation.
 - **Day22**: Solving the slider problem with graph transversal.
 - **Day23**: Day12 int-code, but now with a new instruction that toggles other instructions. Just required a new instructions to be added, but here the `interpreter` code got duplicated for the first time.
 - **Day24**: Two different path finding formulations that amount to TSP. While a inefficient "enumerate all permutations" and check with unitary steps was used, the solution is fast enough compared to the other days. Graph compression can probably drop the solution here by one or two orders of magnitude.
 - **Day25**: Da12 strikes back, but this day problem can probably be reduced to the halting problem if one tries to use the instructions provided to to run the program and brute force the solution. With some assumptions that can be gathered from interpreting the program, one can brute force it. The implemented solution here just leverages understanding what the instruction does (print a number binary representation in reverse order) to find the pertinent constants and just calculate the answer, without having to run the program. A brute-force solution is left along with the analytical solution.


## Performance
This year I've moved from a format with always using one function for part 1 and another for part 2, which helped me to reuse part of the intermediate results. It also reduced boilerplate for most of the days, but testing looks off for others. The first set of solutions run under 10 seconds while solving problems in parallel, having the runtime dominated by day 11. 

``` 
Day 01: 11.50ms
Day 02: 10.26ms
Day 03: 1.79ms
Day 04: 7.61ms
Day 05: 4.50s
Day 06: 1.38ms
Day 07: 5.35ms
Day 08: 11.19ms
Day 09: 9.03ms
Day 10: 9.31ms
Day 11: 8.77s
Day 12: 51.54ms
Day 13: 3.91ms
Day 14: 622.65ms
Day 15: 110.12ms
Day 16: 76.91ms
Day 17: 145.79ms
Day 18: 1.01s
Day 19: 904.24µs
Day 20: 5.48ms
Day 21: 19.19ms
Day 22: 213.81ms
Day 23: 4.98s
Day 24: 13.75ms
Day 25: 674.26µs
Total 8.77s
```

### Lets optimize it!

The 3 slowest day clock at more than 4 secods (days 11, 23 and 5), followed by 5 problems between ~ 100ms to 1s (18, 14, 22, 17 and 15), with the remainder problems taking less than 80ms. Considering that 100ms have an "almost instantaneous feel", this can be a nice target. An intermediate target could be 500 ms, and under 1s for everything should be fast to reach, depending on those MD5 hashes being viable. 

```
Day 11: 8.77s
Day 23: 4.98s
Day 05:  4.50s
Day 18: 1.01s
Day 14: 622.65ms
Day 22: 213.81ms
Day 17: 145.79ms
Day 15: 110.12ms
Day 16: 76.91ms
Day 12: 51.54ms
Day 21: 19.19ms
Day 24: 13.75ms
Day 08: 11.19ms
Day 01: 11.50ms
Day 02: 10.26ms
Day 10: 9.31ms
Day 09: 9.03ms
Day 04: 7.61ms
Day 20: 5.48ms
Day 07: 5.35ms
Day 13: 3.91ms
Day 03: 1.79ms
Day 06: 1.38ms
Day 19: 904.24µs
Day 25: 674.26µs
```

