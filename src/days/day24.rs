use hashbrown::HashMap;
use itertools::Itertools;
use pathfinding::prelude::dijkstra;

type Inputs = (Vec<Vec<bool>>, Vec<(usize, usize, usize)>);

fn parse_input(input: &str) -> Inputs {
    let mut wires: Vec<(usize, usize, usize)> = Vec::new();
    let mut map: Vec<Vec<bool>> = Vec::new();

    std::fs::read_to_string(input)
        .unwrap_or_else(|_| panic!("Failed to read from file: {input}"))
        .lines()
        .enumerate()
        .for_each(|(i, l)| {
            let mut map_line = Vec::new();
            l.chars().enumerate().for_each(|(j, c)| match c {
                '#' => {
                    map_line.push(false);
                }
                '.' => {
                    map_line.push(true);
                }
                _ => {
                    map_line.push(true);
                    wires.push(((c as u8 - b'0') as usize, i, j));
                }
            });
            map.push(map_line.clone());
        });
    wires.sort();
    (map, wires)
}

fn neighbors((x, y): &(usize, usize), (lx, ly): (usize, usize)) -> Vec<(usize, usize)> {
    let arr: [(isize, isize); 4] = [(0, 1), (0, -1), (1, 0), (-1, 0)];
    arr.into_iter()
        .map(|(dx, dy)| {
            if dx < 0 {
                (x.saturating_sub(1), *y)
            } else if dy < 0 {
                (*x, y.saturating_sub(1))
            } else {
                (lx.min(x + dx as usize), ly.min(y + dy as usize))
            }
        })
        .filter(|&p| p != (*x, *y))
        .collect()
}

fn sucessors((i, j): &(usize, usize), map: &[Vec<bool>]) -> Vec<(usize, usize)> {
    let i_max = map.len();
    let j_max = map.first().unwrap().len();
    neighbors(&(*i, *j), (i_max, j_max))
        .iter()
        .filter(|(i, j)| map[*i][*j])
        .copied()
        .collect()
}

fn solution((map, wires): &Inputs) -> (usize, usize) {
    let mut distances: HashMap<(usize, usize), usize> = HashMap::new();
    for i in 1..wires.len() {
        let start = (wires[0_usize].1, wires[0_usize].2);
        let goal = (wires[i].1, wires[i].2);
        if let Some((_, cost)) = dijkstra(
            &start,
            |p| sucessors(p, map).into_iter().map(|p| (p, 1)),
            |&p| p == goal,
        ) {
            distances.insert((0, i), cost);
        } else {
            panic!("Failed to find route from 0 to {i}");
        }
    }

    for i in 1..wires.len() {
        for j in 1..i {
            let start = (wires[i].1, wires[i].2);
            let goal = (wires[j].1, wires[j].2);
            if let Some((_, cost)) = dijkstra(
                &start,
                |p| sucessors(p, map).into_iter().map(|p| (p, 1)),
                |&p| p == goal,
            ) {
                distances.insert((i, j), cost);
                distances.insert((j, i), cost);
            } else {
                panic!("Failed to find route from {i} to {j}");
            }
        }
    }

    let mut p1 = usize::MAX;
    let mut p2 = usize::MAX;

    for p in (1..wires.len()).permutations(wires.len() - 1) {
        let mut arr = vec![0];
        arr.extend_from_slice(&p);
        let mut s = 0;
        for w in arr.windows(2) {
            s += distances
                .get(&(w[0], w[1]))
                .unwrap_or_else(|| panic!("failed for: {w:?}"));
        }

        if s < p1 {
            p1 = s;
        }

        let lastpos = p[p.len() - 1_usize];
        s += distances.get(&(0, lastpos)).unwrap();
        if s < p2 {
            p2 = s;
        }
    }

    (p1, p2)
}

pub fn solve() -> (String, String) {
    let input = parse_input("input/24.txt");
    let (p1, p2) = solution(&input);
    (p1.to_string(), p2.to_string())
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn test_part1() {
        let input = parse_input("testdata/24.txt");
        let (p1, p2) = solution(&input);
        assert_eq!(p1, 14);
        assert_eq!(p2, 20);
    }
}
