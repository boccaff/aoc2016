use crate::assembunny_interpreter;
use crate::parse_assembunny_program;
use crate::Instr;
use crate::Value;

#[allow(dead_code)]
fn brute_force(input: &[Instr]) -> (isize, isize) {
    let mut i = 0;
    loop {
        let (_, pattern) = assembunny_interpreter(input, &[i, 0, 0, 0], 32);
        if pattern.chunks(2).all(|v| v == vec![0, 1]) || pattern.chunks(2).all(|v| v == vec![1, 0])
        {
            break;
        } else {
            i += 1;
        }
    }
    (i, 0)
}

fn solution(input: &[Instr]) -> (isize, isize) {
    let Instr::Cpy(Value::Int(x1), _) = input[1] else {panic!("input does not match expected pattern")};
    let Instr::Cpy(Value::Int(x2), _) = input[2] else {panic!("input does not match expected pattern")};

    let n = x1 * x2;
    let a = n.ilog2();
    let i = n & 1;

    let s: isize = (0..=a)
        .filter(|x| (x & 1) == i as u32)
        .map(|x| 2_isize.pow(x))
        .sum();

    (s - n, 0)
}

pub fn solve() -> (String, String) {
    let input = parse_assembunny_program("input/25.txt");
    let (p1, p2) = solution(&input);
    (p1.to_string(), p2.to_string())
}
