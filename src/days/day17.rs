use base16ct;
use md5::{Digest, Md5};
use std::collections::HashSet;

use pathfinding::prelude::dijkstra;

const GOAL: (isize, isize) = (3, 3);

fn parse_input(input: &str) -> String {
    std::fs::read_to_string(input)
        .unwrap_or_else(|_| panic!("Failed to read from file: {input}"))
        .trim()
        .to_string()
}

fn calcpath(path: &[u8]) -> Option<(isize, isize)> {
    let mut coord = (0, 0);
    for c in path {
        match *c as char {
            'D' => coord.0 += 1,
            'R' => coord.1 += 1,
            'L' => coord.1 -= 1,
            'U' => coord.0 -= 1,
            _ => panic!(
                "Got {} as part of the path: {:?}",
                *c as char,
                std::str::from_utf8(path)
            ),
        }
        if coord.0 < 0 || coord.0 > 3 || coord.1 < 0 || coord.1 > 3 {
            return None;
        }
    }
    Some(coord)
}

fn checkdoors(input: &str) -> Vec<u8> {
    let mut hasher = Md5::new();
    hasher.update(input);
    let hash = hasher.finalize();
    let x = base16ct::lower::encode_string(&hash);
    x.bytes()
        .take(4)
        .map(|c| (b'b'..=b'f').contains(&c))
        .zip("UDLR".bytes())
        .filter(|(b, _)| *b)
        .map(|(_, c)| c)
        .collect()
}

fn sucessors(input: &[u8], l: usize) -> Vec<(Vec<u8>, usize)> {
    let pass = std::str::from_utf8(input).unwrap();
    checkdoors(pass)
        .iter()
        .map(|c| {
            let mut v = input.to_vec();
            v.push(*c);
            v
        })
        .map(|path| (calcpath(&path[l..]), path))
        .filter(|(x, _)| x.is_some())
        .map(|(_, x)| (x, 1))
        .collect()
}

fn is_goal(path: &[u8], goal: (isize, isize)) -> bool {
    let coords = calcpath(path);
    if let Some(p) = coords {
        p == goal
    } else {
        false
    }
}

fn findpath(input: &str) -> String {
    let start: Vec<u8> = input.bytes().collect();
    let l = input.len();
    let (path, _) = dijkstra(&start, |p| sucessors(p, l), |p| is_goal(&p[l..], GOAL)).unwrap();
    let path = &path.last().unwrap()[l..];
    let path = std::str::from_utf8(path).unwrap();
    path.to_string()
}

fn largestpath(input: &str) -> Option<usize> {
    let mut queue = Vec::new();
    let mut seen = HashSet::new();
    let mut solution: Option<Vec<u8>> = None;
    let l = input.len();
    queue.push(input.bytes().collect::<Vec<u8>>());

    while let Some(candidate) = queue.pop() {
        if is_goal(&candidate[l..], GOAL) {
            if let Some(ref current) = solution {
                if candidate.len() > current.len() {
                    solution = Some(candidate.clone());
                }
            } else {
                solution = Some(candidate.clone());
            }
        } else {
            for (neighbor, _) in sucessors(&candidate, l) {
                if !seen.contains(&neighbor) && calcpath(&neighbor[l..]).is_some() {
                    seen.insert(neighbor.clone());
                    queue.push(neighbor);
                }
            }
        }
    }

    solution.map(|result| result.len() - l)
}

fn solution(input: &str) -> (String, usize) {
    let p1 = findpath(input);
    let p2 = largestpath(input).unwrap_or(0);
    (p1, p2)
}

pub fn solve() -> (String, String) {
    let input = parse_input("input/17.txt");
    let (p1, p2) = solution(&input);
    (p1.to_string(), p2.to_string())
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn test_part1() {
        let input = parse_input("testdata/17a.txt");
        let p1 = findpath(&input);
        assert_eq!(p1, "DDRRRD");
        let p2 = largestpath(&input);
        assert_eq!(p2, Some(370));
        let input = parse_input("testdata/17b.txt");
        let p1 = findpath(&input);
        assert_eq!(p1, "DDUDRLRRUDRD");
        let input = parse_input("testdata/17c.txt");
        let p1 = findpath(&input);
        assert_eq!(p1, "DRURDRUDDLLDLUURRDULRLDUUDDDRR");
    }
}
