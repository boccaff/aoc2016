use scan_fmt::scan_fmt;

fn parse_line(elem: &str) -> [usize; 3] {
    match scan_fmt!(
        elem,
        "Disc #{d} has {d} positions; at time={d}, it is at position {d}.",
        usize,
        usize,
        usize,
        usize
    ) {
        Ok((disc, positions, 0, position)) => [disc, positions, position],
        Ok(_) => panic!(
            "Failed to parse: '{}', with position at non-zero time",
            elem
        ),
        Err(e) => panic!("Failed to parse: '{}', with error: {:}", elem, e),
    }
}

fn parse_input(input: &str) -> Vec<[usize; 3]> {
    std::fs::read_to_string(input)
        .unwrap_or_else(|_| panic!("Failed to read from file: {input}"))
        .lines()
        .map(parse_line)
        .collect()
}

fn calc_position(&[disc, n, pos]: &[usize; 3], i: usize) -> usize {
    (disc + i + pos).rem_euclid(n)
}

fn solution(input: &[[usize; 3]]) -> usize {
    let mut i = 0;
    loop {
        let arr: Vec<usize> = input.iter().map(|arr| calc_position(arr, i)).collect();
        if arr.iter().skip(1).all(|&x| x == arr[0]) {
            break;
        } else {
            i += 1;
        }
    }
    i
}

pub fn solve() -> (String, String) {
    let input = parse_input("input/15.txt");
    let p1 = solution(&input);
    let input = parse_input("input/15b.txt");
    let p2 = solution(&input);
    (p1.to_string(), p2.to_string())
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn test_part1() {
        let input = parse_input("testdata/15.txt");
        let p1 = solution(&input);
        assert_eq!(p1, 5);
        // assert_eq!(p2, 0);
    }
}
