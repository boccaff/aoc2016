use scan_fmt::scan_fmt;

fn parse_bot(elem: &str) -> (usize, bool, usize, bool, usize) {
    match scan_fmt!(
        elem,
        "bot {d} gives low to {[outputbot]} {d} and high to {[outputbot]} {d}",
        usize,
        String,
        usize,
        String,
        usize
    ) {
        Ok((bot, lowtobot, low_bot, hightobot, high_bot)) => (
            bot,
            lowtobot.starts_with('b'),
            low_bot,
            hightobot.starts_with('b'),
            high_bot,
        ),
        Err(e) => panic!("Failed to parse: '{}', with error: {:}", elem, e),
    }
}

fn parse_value(elem: &str) -> (usize, usize) {
    match scan_fmt!(elem, "value {d} goes to bot {d}", usize, usize) {
        Ok((value, bot)) => (value, bot),
        Err(e) => panic!("Failed to parse: '{}', with error: {:}", elem, e),
    }
}

type Inputs = (Vec<(usize, bool, usize, bool, usize)>, Vec<(usize, usize)>);

fn parse_input(input: &str) -> Inputs {
    let mut bots: Vec<(usize, bool, usize, bool, usize)> = Vec::new();
    let mut values: Vec<(usize, usize)> = Vec::new();

    std::fs::read_to_string(input)
        .unwrap()
        .lines()
        .for_each(|line| {
            if line.starts_with("bot") {
                let (bot, lowtobot, lowto, hightobot, highto) = parse_bot(line);
                bots.push((bot, lowtobot, lowto, hightobot, highto));
            } else {
                let (bot, value) = parse_value(line);
                values.push((bot, value));
            }
        });

    bots.sort_by_key(|k| k.0);
    let bots = bots.into_iter().collect::<Vec<_>>();

    (bots, values)
}

fn solution((bots, inputs): &Inputs, p1_ans: (usize, usize)) -> (usize, usize) {
    let bots = bots.to_vec();
    let mut outputs: Vec<usize> = vec![0; bots.len()];
    let mut storage: Vec<(Option<usize>, Option<usize>)> = vec![(None, None); bots.len()];
    let mut p1: Option<usize> = None;

    inputs.iter().for_each(|(value, bot_id)| {
        if storage[*bot_id].0.is_none() {
            storage[*bot_id].0 = Some(*value);
        } else if storage[*bot_id].1.is_none() {
            storage[*bot_id].1 = Some(*value);
        } else {
            panic!("Tried to set {value} for {:?}", storage[*bot_id])
        }
    });

    loop {
        for i in 0..bots.len() {
            if storage[i].0.is_some() && storage[i].1.is_some() {
                let (_, ltb, lb, htb, hb) = bots[i];

                let Some(mut l) = storage[i].0 else {unreachable!()};
                let Some(mut h) = storage[i].1 else {unreachable!()};
                storage[i].0 = None;
                storage[i].1 = None;
                if l > h {
                    (l, h) = (h, l)
                }
                if (l, h) == p1_ans {
                    p1 = Some(i);
                }

                if ltb {
                    if storage[lb].0.is_none() {
                        storage[lb].0 = Some(l);
                    } else {
                        storage[lb].1 = Some(l);
                    }
                } else {
                    outputs[lb] = l;
                }

                if htb {
                    if storage[hb].0.is_none() {
                        storage[hb].0 = Some(h);
                    } else {
                        storage[hb].1 = Some(h);
                    }
                } else {
                    outputs[hb] = h;
                }
            }
        }
        if storage.iter().all(|(a, b)| a.is_none() & b.is_none()) {
            break;
        }
    }

    let Some(p1 ) = p1 else {panic!("Failed to solve P1")} ;
    let p2 = outputs.iter().take(3).product::<usize>();
    (p1, p2)
}

pub fn solve() -> (String, String) {
    let input = parse_input("input/10.txt");
    let (p1, p2) = solution(&input, (17, 61));
    (p1.to_string(), p2.to_string())
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn test_solution() {
        let input = parse_input("testdata/10.txt");
        let (p1, p2) = solution(&input, (2, 5));
        assert_eq!(p1, 2);
        assert_eq!(p2, 30);
    }
}
