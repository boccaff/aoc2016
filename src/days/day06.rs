use std::usize;

fn parse_input(input: &str) -> Vec<String> {
    std::fs::read_to_string(input)
        .unwrap()
        .lines()
        .map(|x| x.to_owned())
        .collect()
}

fn solution(input: &[String], n: usize) -> (String, String) {
    let mut chars = vec![vec![0_isize; 26]; n];
    input.iter().for_each(|l| {
        l.chars().enumerate().for_each(|(i, x)| {
            chars[i][(x as u8 - b'a') as usize] += 1;
        })
    });

    let mut p1: Vec<char> = Vec::new();
    let mut p2: Vec<char> = Vec::new();

    chars.into_iter().for_each(|arr| {
        let mut new_arr: Vec<(isize, usize)> =
            arr.into_iter().enumerate().map(|(i, x)| (x, i)).collect();
        new_arr.sort();

        p1.push((new_arr.iter().map(|(_, i)| *i).last().unwrap() as u8 + b'a') as char);
        p2.push(
            (new_arr
                .iter()
                .filter(|(x, _)| *x != 0)
                .map(|(_, i)| *i)
                .rev()
                .last()
                .unwrap() as u8
                + b'a') as char,
        );
    });
    (p1.iter().collect(), p2.iter().collect())
}

pub fn solve() -> (String, String) {
    let input = parse_input("input/06.txt");
    let (p1, p2) = solution(&input, 8);
    (p1.to_string(), p2.to_string())
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn test_part1() {
        let input = parse_input("testdata/06.txt");
        let (p1, _) = solution(&input, 6);
        assert_eq!(p1, "easter".to_string());
    }

    #[test]
    fn test_part2() {
        let input = parse_input("testdata/06.txt");
        let (_, p2) = solution(&input, 6);
        assert_eq!(p2, "advent".to_string());
    }
}
