use base16ct;
use hashbrown::hash_map::HashMap;
use md5::{Digest, Md5};
use rayon::iter::{ParallelBridge, ParallelIterator};
use std::collections::hash_set::HashSet;

pub fn parse_input(input: &str) -> String {
    std::fs::read_to_string(input).unwrap().trim().to_string()
}

fn find_n_repeats(v: &[u8], n: usize) -> Option<u8> {
    let p = v
        .windows(n)
        .position(|x| x.iter().skip(1).all(|&xi| xi == x[0_usize]));
    p.map(|i| v[i])
}

fn generate_hashes(
    salt: &String,
    i: usize,
    batch_size: usize,
    stretching: usize,
    hashfoo: fn(&String, usize) -> String,
) -> Vec<(usize, Option<u8>, Option<u8>)> {
    let mut res = (0..batch_size)
        .par_bridge()
        .map(|x| {
            let p = format!("{}{}", salt, x + i);
            let hash = hashfoo(&p, stretching).bytes().collect::<Vec<u8>>();

            let quintuplets = find_n_repeats(&hash, 5);
            let triplets = find_n_repeats(&hash, 3);
            (x + i, triplets, quintuplets)
        })
        .collect::<Vec<_>>();
    res.sort();
    res
}

fn f1(x: &String, n: usize) -> String {
    let mut x: String = x.to_owned();
    for _ in 0..n {
        let mut hasher = Md5::new();
        hasher.update(&x);
        let hash = hasher.finalize();
        x = base16ct::lower::encode_string(&hash);
    }
    x
}

pub fn generate_keys(input: &String, stretching: usize) -> usize {
    let mut i = 0;
    let mut keys: HashSet<usize> = HashSet::new();
    let mut trees: HashMap<u8, Vec<usize>> = HashMap::new();
    let mut fives: HashMap<u8, Vec<usize>> = HashMap::new();
    let batchsize = 512;

    loop {
        let hashes = generate_hashes(input, i, batchsize, stretching, f1);
        for (j, triplets, quintuplets) in hashes {
            if let Some(t) = triplets {
                let v = trees.entry(t).or_insert(Vec::new());
                v.push(j);
            }

            if let Some(q) = quintuplets {
                let v = fives.entry(q).or_insert(Vec::new());
                v.push(j);

                if trees.contains_key(&q) {
                    trees.get(&q).unwrap().iter().for_each(|t| {
                        if t + 1000 > j && j > *t {
                            keys.insert(*t);
                        }
                    });
                }

                if fives.contains_key(&q) {
                    fives.get(&q).unwrap().iter().for_each(|t| {
                        if t + 1000 > j && j > *t {
                            keys.insert(*t);
                        }
                    });
                }
            }
        }
        if keys.len() >= 64 {
            break;
        } else {
            i += batchsize;
        }
    }

    let mut v: Vec<usize> = keys.into_iter().collect();
    v.sort();
    
    v[63_usize]
}

fn solution(input: &String) -> (usize, usize) {
    let p1 = generate_keys(input, 1);
    let p2 = generate_keys(input, 2017);
    (p1, p2)
}

pub fn solve() -> (String, String) {
    let input = parse_input("input/14.txt");
    let (p1, p2) = solution(&input);
    (p1.to_string(), p2.to_string())
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    #[ignore]
    fn test_part1() {
        let input = parse_input("testdata/14.txt");
        let (p1, p2) = solution(&input);
        assert_eq!(p1, 22_728);
        assert_eq!(p2, 22_551);
    }
}
