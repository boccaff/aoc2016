use crate::assembunny_interpreter;
use crate::parse_assembunny_program;
use crate::{Instr, Value};

fn solution(input: &[Instr]) -> (isize, isize) {
    let (p1, _) = assembunny_interpreter(input, &[7, 0, 0, 0], 2);
    let (p2, _) = assembunny_interpreter(input, &[12, 0, 0, 0], 2);
    (p1[0_usize], p2[0_usize])
}

pub fn solve() -> (String, String) {
    let mut input = parse_assembunny_program("input/23.txt");
    input[5] = Instr::Mul(Value::Reg(2), Value::Reg(3));
    input[6] = Instr::Add(Value::Reg(3), Value::Reg(0));
    input[7] = Instr::Cpy(Value::Int(0), Value::Reg(2));
    input[8] = Instr::Cpy(Value::Int(0), Value::Reg(3));
    input[9] = Instr::Nop;
    let (p1, p2) = solution(&input);
    (p1.to_string(), p2.to_string())
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn test_part1() {
        let input = parse_assembunny_program("testdata/23.txt");
        let (p1, _) = solution(&input);
        assert_eq!(p1, 3);
    }
}
