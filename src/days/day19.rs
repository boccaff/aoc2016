fn parse_input(input: &str) -> usize {
    std::fs::read_to_string(input)
        .unwrap_or_else(|_| panic!("Failed to read from file: {input}"))
        .trim()
        .parse::<usize>()
        .unwrap()
}

#[allow(dead_code)]
fn brute_part1(n: usize) -> usize {
    let mut v = vec![1_usize; n];
    let mut i = 0;

    loop {
        if v[i] != 0 {
            let mut j = (i + 1).rem_euclid(n);
            while v[j] == 0 {
                j = (j + 1).rem_euclid(n);
            }
            v[i] += v[j];
            v[j] = 0;
        }
        if v[i] == n {
            return i + 1;
        }
        i = (i + 1).rem_euclid(n);
    }
}

#[allow(dead_code)]
fn brute_part2(n: usize) -> usize {
    let mut v = vec![1_usize; n];
    let mut i = 0;
    let mut l = n;

    // yap! this is bad
    while l > 0 {
        if v[i] != 0 {
            let mut t = 0;
            let mut j = (i + 1).rem_euclid(n);
            loop {
                if v[j] != 0 {
                    t += 1;
                }
                if t >= l / 2 {
                    break;
                } else {
                    j = (j + 1).rem_euclid(n);
                }
            }
            v[i] += v[j];
            v[j] = 0;
            l -= 1;
        }
        if v[i] == n {
            return i + 1;
        }
        i = (i + 1).rem_euclid(n);
    }
    unreachable!()
}

fn part1(input: usize) -> usize {
    let b = input.ilog2();
    let mut x = input - 2_usize.pow(b);
    x <<= 1;
    x + 1
}

fn part2(input: usize) -> usize {
    let mut k = 0;

    while 3_usize.pow((k + 1) as u32) < input {
        k += 1;
    }
    let b = 3_usize.pow(k as u32);
    let d = input - b;
    if d <= b {
        d
    } else {
        b + (d - b) * 2
    }
}

fn solution(input: usize) -> (usize, usize) {
    let p1 = part1(input);
    let p2 = part2(input);
    (p1, p2)
}

pub fn solve() -> (String, String) {
    let input = parse_input("input/19.txt");
    // let input = parse_input("testdata/19b.txt");
    let (p1, p2) = solution(input);
    (p1.to_string(), p2.to_string())
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn test_part1() {
        let input = parse_input("testdata/19.txt");
        assert_eq!(brute_part1(input), 3);
        assert_eq!(brute_part2(input), 2);

        for i in 2..100 {
            assert_eq!(part1(i), brute_part1(i));
            assert_eq!(part2(i), brute_part2(i));
        }
    }
}
