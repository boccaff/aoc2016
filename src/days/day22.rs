use pathfinding::prelude::dijkstra;
use scan_fmt::scan_fmt;

fn parse_input(input: &str) -> Vec<[usize; 4]> {
    std::fs::read_to_string(input)
        .unwrap_or_else(|_| panic!("Failed to read from file: {input}"))
        .lines()
        .skip(2)
        .filter_map(parse_line)
        .collect()
}

fn parse_line(line: &str) -> Option<[usize; 4]> {
    if let Ok((a, b, c, d, _)) = scan_fmt!(
        line,
        "/dev/grid/node-x{}-y{}\t{}T\t{}T{}",
        usize,
        usize,
        usize,
        usize,
        String
    ) {
        Some([a, b, c, d])
    } else {
        None
    }
}

fn movements(p: usize, m: usize, n: usize) -> Vec<usize> {
    let mut res = Vec::with_capacity(4);
    let is_left = (p).rem_euclid(m) == 0;
    let is_right = (p + 1).rem_euclid(m) == 0;
    let is_top = p / m == 0;
    let is_bottom = p / m == n - 1;

    if !is_top {
        res.push(p - m);
    }

    if !is_left {
        res.push(p - 1);
    }

    if !is_right {
        res.push(p + 1);
    }

    if !is_bottom {
        res.push(p + m);
    }

    res
}

fn sucessors((p, g): (usize, usize), m: usize, n: usize, i: &[usize]) -> Vec<(usize, usize)> {
    movements(p, m, n)
        .iter()
        .filter(|n| !i.contains(n))
        .map(|&n| if n == g { (g, p) } else { (n, g) })
        .collect()
}

fn solution(input: &[[usize; 4]]) -> (usize, isize) {
    let mut p1 = 0;
    for i in 0..input.len() {
        for j in 0..input.len() {
            if (i != j) && (input[i][3] != 0) && (input[i][3] <= input[j][2] - input[j][3]) {
                p1 += 1;
            }
        }
    }

    let m = input.iter().map(|v| v[1]).max().unwrap() + 1;
    let n = input.iter().map(|v| v[0]).max().unwrap() + 1;

    let immovable: Vec<usize> = input
        .iter()
        .filter(|v| v[3] > 100)
        .map(|v| v[0] + v[1] * n)
        .collect();
    let empty_i = input.iter().position(|v| v[3] == 0).unwrap();
    let empty = input[empty_i][0] + input[empty_i][1] * n;

    let results = dijkstra(
        &(empty, n - 1),
        |x| sucessors(*x, n, m, &immovable).into_iter().map(|p| (p, 1)),
        |&x| x.1 == 0,
    );

    if let Some((_, cost)) = results {
        (p1, cost)
    } else {
        panic!("Failed to find path!")
    }
}

#[allow(dead_code)]
fn print_board(m: usize, n: usize, g: usize, p: usize, immovable: &[usize]) {
    println!("{:?}", (m, n, g, p));
    print!("   ");
    for i in 0..n {
        print!(" {i:02}");
    }
    println!();
    for i in 0..m {
        print!(" {i:02} ");
        for j in 0..n {
            let x = j + i * n;
            if x == p {
                print!(" _ ");
            } else if x == g {
                print!(" G ");
            } else if immovable.contains(&x) {
                print!(" # ");
            } else {
                print!(" . ");
            }
        }
        println!();
    }
}

pub fn solve() -> (String, String) {
    let input = parse_input("input/22.txt");
    let (p1, p2) = solution(&input);
    (p1.to_string(), p2.to_string())
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn test_part1() {
        let input = parse_input("input/22.txt");
        assert_eq!(input.len(), 1015)
    }
}
