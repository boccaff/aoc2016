const LPARENS: u8 = b'(';
const RPARENS: u8 = b')';
const X: u8 = b'x';
const ZERO: u8 = b'0';

fn parse_input(input: &str) -> Vec<Vec<u8>> {
    std::fs::read_to_string(input)
        .unwrap()
        .lines()
        .map(|line| line.bytes().collect())
        .collect()
}

fn parse_block(v: &[u8]) -> (usize, usize) {
    let mut a = 0;
    let mut b = 0;
    let mut i = 0;
    let mut past_x = false;
    let l = v.len();

    for x in v.iter().take(l - 1).skip(1).rev() {
        if *x == X {
            past_x = true;
            i = 0;
        } else if past_x {
            a += (x - ZERO) as usize * 10_usize.pow(i);
            i += 1;
        } else {
            b += (x - ZERO) as usize * 10_usize.pow(i);
            i += 1;
        }
    }
    (a, b)
}

fn decompress(line: &[u8]) -> Vec<u8> {
    let mut res: Vec<u8> = Vec::new();
    let l = line.len();
    let mut i = 0_usize;

    while i < l {
        let c = line[i];
        if c == LPARENS {
            let j = line[i..].iter().position(|x| *x == RPARENS).unwrap();
            let end_block = i + j;
            let (chars, times) = parse_block(&line[i..(end_block + 1)]);
            for _ in 0..times {
                res.extend_from_slice(&line[(end_block + 1)..(end_block + 1 + chars)])
            }
            i = end_block + 1 + chars;
        } else {
            i += 1;
            res.push(c);
        }
    }
    res
}

fn calc_decompress_size(line: &[u8]) -> usize {
    let l = line.len();
    let mut i = 0_usize;
    let mut s = 0_usize;

    while i < l {
        let c = line[i];
        if c == LPARENS {
            let j = line[i..].iter().position(|x| *x == RPARENS).unwrap();
            let end_block = i + j;
            let (chars, times) = parse_block(&line[i..(end_block + 1)]);
            s += times * calc_decompress_size(&line[(end_block + 1)..(end_block + 1 + chars)]);
            i = end_block + 1 + chars;
        } else {
            i += 1;
            s += 1;
        }
    }
    s
}

fn solution(input: &[Vec<u8>]) -> (usize, usize) {
    let mut p1 = 0;
    let mut p2 = 0;
    for i in input {
        p1 += decompress(i).len();
        p2 += calc_decompress_size(i);
    }
    (p1, p2)
}

pub fn solve() -> (String, String) {
    let input = parse_input("input/09.txt");
    let (p1, p2) = solution(&input);
    (p1.to_string(), p2.to_string())
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn test_parse_block() {
        assert_eq!(parse_block(&[LPARENS, 51, X, 50, RPARENS]), (3, 2));
        assert_eq!(parse_block(&[LPARENS, 56, X, 52, RPARENS]), (8, 4))
    }

    #[test]
    fn test_part1() {
        let input = parse_input("testdata/09a.txt");
        let (p1, _) = solution(&input);
        assert_eq!(p1, 57);
    }

    #[test]
    fn test_part2() {
        let input = parse_input("testdata/09b.txt");
        let (_, p2) = solution(&input);
        assert_eq!(p2, 242394);
    }
}
