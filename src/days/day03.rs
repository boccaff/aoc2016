use crate::unsigned_int_vec;

fn parse_input(input: &str) -> Vec<Vec<isize>> {
    std::fs::read_to_string(input)
        .unwrap()
        .lines()
        .map(unsigned_int_vec)
        .collect()
}

fn is_proper_triangle(v: &[isize]) -> bool {
    let s: isize = v.iter().sum();
    let m: isize = *v.iter().max().unwrap();
    let ps = s - m;

    ps > m
}

fn build_vec(input: &[Vec<isize>], i: usize, j: usize) -> Vec<isize> {
    vec![input[i][j], input[i + 1_usize][j], input[i + 2_usize][j]]
}

fn solution(input: &[Vec<isize>]) -> (isize, isize) {
    let p1 = input.iter().filter(|v| is_proper_triangle(v)).count();
    let p2: isize = (0..input.len())
        .step_by(3)
        .map(|i| {
            let mut s = 0;
            for j in 0_usize..3_usize {
                if is_proper_triangle(&build_vec(input, i, j)) {
                    s += 1;
                }
            }
            s
        })
        .sum();
    (p1 as isize, p2)
}

pub fn solve() -> (String, String) {
    let input = parse_input("input/03.txt");
    let (p1, p2) = solution(&input);
    (p1.to_string(), p2.to_string())
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn test_part1() {
        let input = parse_input("testdata/03.txt");
        let (p1, _) = solution(&input);
        assert_eq!(p1, 1);
    }

    #[test]
    fn test_part2() {
        let input = parse_input("testdata/03.txt");
        let (_, p2) = solution(&input);
        assert_eq!(p2, 2);
    }
}
