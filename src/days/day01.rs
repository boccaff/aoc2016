use nalgebra::base::{Matrix2, Vector2};
use scan_fmt::scan_fmt;

use std::collections::hash_set::HashSet;

fn parse_elem(elem: &str) -> (char, isize) {
    match scan_fmt!(elem, "{[RL]}{d}", char, isize) {
        Ok((dir, steps)) => (dir, steps),
        Err(e) => panic!("Invalid element on list: '{}', with error: {:}", elem, e),
    }
}

fn parse_input(input: &str) -> Vec<(char, isize)> {
    std::fs::read_to_string(input)
        .unwrap()
        .split(", ")
        .map(parse_elem)
        .collect()
}

const R: Matrix2<isize> = Matrix2::new(0, 1, -1, 0);
const L: Matrix2<isize> = Matrix2::new(0, -1, 1, 0);

fn solution(input: &[(char, isize)]) -> (isize, isize) {
    let mut v;
    let mut v2 = Vector2::new(0, 0);
    let mut p = Vector2::new(0, 0);
    let mut h: HashSet<Vector2<isize>> = HashSet::new();

    let (d, s) = input.first().unwrap();
    if *d == 'R' {
        v = Vector2::new(1, 0);
    } else {
        v = Vector2::new(-1, 0);
    }
    for _ in 0..*s {
        p += v;
        h.insert(p);
    }

    for (di, si) in input.iter().skip(1) {
        if *di == 'R' {
            v = R * v;
        } else {
            v = L * v;
        }

        for _ in 0..*si {
            p += v;
            if h.contains(&p) & (v2.abs().sum() == 0) {
                v2 = p;
            }
            h.insert(p);
        }
    }

    (p.abs().sum(), v2.abs().sum())
}

pub fn solve() -> (String, String) {
    let input = parse_input("input/01.txt");
    let (p1, p2) = solution(&input);
    (p1.to_string(), p2.to_string())
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn test_part1() {
        let input = parse_input("testdata/01a.txt");
        let (p1, _) = solution(&input);
        assert_eq!(p1, 5);

        let input = parse_input("testdata/01b.txt");
        let (p1, _) = solution(&input);
        assert_eq!(p1, 2);

        let input = parse_input("testdata/01c.txt");
        let (p1, _) = solution(&input);
        assert_eq!(p1, 12);
    }

    #[test]
    fn test_part2() {
        let input = parse_input("testdata/01d.txt");
        let (_, p2) = solution(&input);
        assert_eq!(p2, 4);
    }
}
