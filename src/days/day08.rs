fn print_board(board: &[Vec<bool>]) {
    board.iter().for_each(|l| {
        l.iter().for_each(|c| {
            if *c {
                print!("#");
            } else {
                print!(" ");
            }
        });
        println!();
    });
    println!();
}

fn parse_line(line: &str) -> (usize, usize, usize) {
    if let Some(stripped) = line.strip_prefix("rect ") {
        let (x, y) = stripped.split_once('x').unwrap();
        let x = x.parse::<usize>().unwrap();
        let y = y.parse::<usize>().unwrap();
        (0_usize, x, y)
    } else if let Some(stripped) = line.strip_prefix("rotate column x=") {
        let (x, y) = stripped.split_once(" by ").unwrap();
        let x = x.parse::<usize>().unwrap();
        let y = y.parse::<usize>().unwrap();
        (1_usize, x, y)
    } else if let Some(stripped) = line.strip_prefix("rotate row y=") {
        let (x, y) = stripped.split_once(" by ").unwrap();
        let x = x.parse::<usize>().unwrap();
        let y = y.parse::<usize>().unwrap();
        (2_usize, x, y)
    } else {
        unreachable!()
    }
}

fn parse_input(input: &str) -> Vec<(usize, usize, usize)> {
    std::fs::read_to_string(input)
        .unwrap()
        .lines()
        .map(parse_line)
        .collect()
}

fn solution(input: &[(usize, usize, usize)], boardsize: (usize, usize)) -> (usize, isize) {
    let (m, n) = boardsize;
    let mut display = vec![vec![false; n]; m];
    let mut from: Vec<(usize, usize)> = Vec::new();
    let mut to: Vec<(usize, usize)> = Vec::new();

    input.iter().for_each(|(p, a, b)| {
        match p {
            0_usize => {
                for row in display.iter_mut().take(*b) {
                    for elem in row.iter_mut().take(*a) {
                        *elem = true;
                    }
                }
            }
            1_usize => {
                for (i, row) in display.iter().enumerate().take(m) {
                    if row[*a] {
                        from.push((i, *a));
                        to.push(((i + *b).rem_euclid(m), *a));
                    }
                }
            }
            2_usize => {
                for j in 0..n {
                    if display[*a][j] {
                        from.push((*a, j));
                        to.push((*a, (j + b).rem_euclid(n)));
                    }
                }
            }
            _ => {
                unreachable!()
            }
        }
        from.iter().for_each(|(i, j)| {
            display[*i][*j] = false;
        });
        to.iter().for_each(|(i, j)| {
            display[*i][*j] = true;
        });
        from = Vec::new();
        to = Vec::new();
    });
    print_board(&display);
    let p = display
        .iter()
        .map(|l| l.iter().filter(|x| **x).count())
        .sum();

    (p, 0)
}

pub fn solve() -> (String, String) {
    let input = parse_input("input/08.txt");
    let (p1, p2) = solution(&input, (6, 50));
    (p1.to_string(), p2.to_string())
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn test_part1() {
        let input = parse_input("testdata/08.txt");
        let (p1, _) = solution(&input, (3, 7));
        assert_eq!(p1, 6);
    }

    #[test]
    fn test_part2() {
        let input = parse_input("testdata/08.txt");
        let (_, p2) = solution(&input, (3, 7));
        assert_eq!(p2, 0);
    }
}
