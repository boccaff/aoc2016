fn parse_input(input: &str) -> Vec<Vec<(bool, Vec<u8>)>> {
    std::fs::read_to_string(input)
        .unwrap()
        .lines()
        .map(parse_line)
        .collect()
}

fn parse_line(line: &str) -> Vec<(bool, Vec<u8>)> {
    let mut res = Vec::new();
    let mut tmp: Vec<u8> = Vec::new();
    let mut outside_brackets = true;

    line.chars().for_each(|c| {
        if c == '[' {
            res.push((outside_brackets, tmp.clone()));
            outside_brackets = false;
            tmp = Vec::new();
        } else if c == ']' {
            res.push((outside_brackets, tmp.clone()));
            outside_brackets = true;
            tmp = Vec::new();
        } else {
            tmp.push(c as u8);
        }
    });
    res.push((outside_brackets, tmp.clone()));

    res
}

fn is_abba(arr: &[u8]) -> bool {
    (arr[0_usize] == arr[3_usize])
        && (arr[1_usize] == arr[2_usize])
        && (arr[0_usize] != arr[1_usize])
}

fn check_for_abba(arr: &[u8]) -> bool {
    arr.windows(4).map(is_abba).any(|x| x)
}

fn check_tls(seq: &[(bool, Vec<u8>)]) -> bool {
    let mut abba_inside = false;
    let mut abba_outside = false;

    for (outside, arr) in seq {
        if outside & check_for_abba(arr) {
            abba_outside = true;
        }

        if !outside & check_for_abba(arr) {
            abba_inside = true;
        }
    }
    !abba_inside & abba_outside
}

fn is_aba(arr: &[u8]) -> bool {
    (arr[0_usize] == arr[2_usize]) & (arr[0_usize] != arr[1_usize])
}

fn aba_bab_match(arr1: &[u8], arr2: &[u8]) -> bool {
    (arr1[0_usize] == arr2[1_usize]) && (arr1[1_usize] == arr2[0_usize])
}

fn check_ssl(seq: &[(bool, Vec<u8>)]) -> bool {
    let outsides: Vec<Vec<u8>> = seq
        .iter()
        .filter(|(outside, _)| *outside)
        .map(|(_, seq)| seq.clone())
        .collect();
    let insides: Vec<Vec<u8>> = seq
        .iter()
        .filter(|(outside, _)| !outside)
        .map(|(_, seq)| seq.clone())
        .collect();
    for o in &outsides {
        for i in &insides {
            for arr1 in o.windows(3) {
                for arr2 in i.windows(3) {
                    if is_aba(arr1) && is_aba(arr2) && aba_bab_match(arr1, arr2) {
                        return true;
                    }
                }
            }
        }
    }
    false
}

fn solution(input: &[Vec<(bool, Vec<u8>)>]) -> (isize, isize) {
    let mut p1 = 0;
    let mut p2 = 0;
    input.iter().for_each(|ip| {
        if check_tls(ip) {
            p1 += 1;
        }

        if check_ssl(ip) {
            p2 += 1;
        }
    });

    (p1, p2)
}
// 395 is too high

pub fn solve() -> (String, String) {
    let input = parse_input("input/07.txt");
    let (p1, p2) = solution(&input);
    (p1.to_string(), p2.to_string())
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_part1() {
        let input = parse_input("testdata/07a.txt");
        let (p1, _) = solution(&input);
        assert_eq!(p1, 2);
    }

    #[test]
    fn test_part2() {
        let input = parse_input("testdata/07b.txt");
        let (_, p2) = solution(&input);
        assert_eq!(p2, 3);
    }
}
