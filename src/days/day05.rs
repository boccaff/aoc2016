use crypto::digest::Digest;
use crypto::md5::Md5;

use std::sync::atomic::Ordering;
use std::sync::{atomic::AtomicBool, mpsc, Arc};
use std::thread;

pub fn parse_input(input: &str) -> String {
    std::fs::read_to_string(input).unwrap().trim().to_string()
}

fn to_char(num: u8) -> char {
    if num < 10 {
        (num + 48) as char
    } else {
        (num + 87) as char
    }
}

pub fn solution(input: &String) -> (String, String) {
    let input = input.to_owned();
    let mut digits_p1: Vec<(usize, char)> = Vec::new();
    let mut digits_p2: Vec<(usize, usize, char)> = Vec::new();
    let mut matches_p2: Vec<bool> = vec![false; 8];
    let is_solution_found = Arc::new(AtomicBool::new(false));

    let n_workers = 10;

    let (tx, rx) = mpsc::sync_channel(10);

    for start in 0..n_workers {
        let sender = tx.clone();
        let input_clone = input.clone();
        let is_solution_found = is_solution_found.clone();

        thread::spawn(move || {
            for salt in (start..).step_by(n_workers) {
                let mut hasher = Md5::new();
                let mut digest = [0_u8; 16];
                hasher.input_str(&input_clone);
                hasher.input_str(&salt.to_string());
                hasher.result(&mut digest);

                if (digest[0] | digest[1] | (digest[2] >> 4)) == 0 {
                    let sixth_digit = to_char(digest[2] & 0xf);
                    let seventh_digit = to_char(digest[3] >> 4);
                    let pos = (sixth_digit as u8 - b'0') as usize;
                    if !is_solution_found.load(Ordering::Relaxed) {
                        if sender.send((salt, sixth_digit, seventh_digit, pos)).is_ok() {};
                    } else {
                        break;
                    }
                }
            }
        });
    }

    loop {
        if matches_p2.iter().all(|x| *x) {
            is_solution_found.store(true, Ordering::Relaxed);
            break;
        }
        let (salt, sixth_digit, seventh_digit, pos) = rx.recv().unwrap();
        digits_p1.push((salt, sixth_digit));

        if pos < 8 {
            digits_p2.push((pos, salt, seventh_digit));
            matches_p2[pos] = true;
        }
    }
    drop(tx);

    digits_p1.sort();
    digits_p2.sort_by_key(|(d, s, _)| (*d, *s));
    digits_p2.dedup_by_key(|(d, _, _)| *d);

    let mut p2: Vec<char> = vec![' '; 8];
    digits_p2.into_iter().for_each(|(d, _, c)| {
        p2[d] = c;
    });

    let p1: String = digits_p1.into_iter().take(8).map(|(_, c)| c).collect();
    let p2: String = p2.into_iter().collect();

    (p1, p2)
}

pub fn solve() -> (String, String) {
    let input = parse_input("input/05.txt");
    let (p1, p2) = solution(&input);
    (p1, p2.to_string())
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    #[ignore]
    fn test_part1() {
        let input = parse_input("testdata/05.txt");
        let (p1, p2) = solution(&input);
        assert_eq!(p1, "18f47a30");
        assert_eq!(p2, "05ace8e3");
    }
}
