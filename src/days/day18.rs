pub fn parse_input(input: &str) -> Vec<usize> {
    std::fs::read_to_string(input)
        .unwrap_or_else(|_| panic!("Failed to read from file: {input}"))
        .trim()
        .chars()
        .rev()
        .map(|c| if c == '^' { 1 } else { 0 })
        .collect()
}

fn create_new_row(input: &[usize]) -> Vec<usize> {
    let f1 = vec![1, 1, 0];
    let f2 = vec![0, 1, 1];
    let f3 = vec![1, 0, 0];
    let f4 = vec![0, 0, 1];

    let mut row = input.to_vec();
    row.insert(0, 0);
    row.push(0);

    let mut out = vec![0; input.len()];

    row.windows(3).enumerate().for_each(|(i, v)| {
        if (v == f1) || (v == f2) || (v == f3) || (v == f4) {
            out[i] = 1;
        } else {
            out[i] = 0;
        }
    });
    out
}

pub fn part1(x: &[usize], n: usize) -> usize {
    let mut s = 0_usize;

    let mut row = x.to_vec();
    s += calc_freetiles(&row);

    for _ in 1..n {
        row = create_new_row(&row);
        s += calc_freetiles(&row);
    }
    s
}

fn calc_freetiles(input: &[usize]) -> usize {
    input.len() - input.iter().sum::<usize>()
}

fn solution(input: &[usize], n1: usize, n2: usize) -> (usize, usize) {
    let p1 = part1(input, n1);
    let p2 = part1(input, n2);
    (p1, p2)
}

pub fn solve() -> (String, String) {
    let input = parse_input("input/18.txt");
    let (p1, p2) = solution(&input, 40, 400_000);
    (p1.to_string(), p2.to_string())
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn test_solution() {
        let input = parse_input("testdata/18a.txt");
        let p1 = part1(&input, 3);
        assert_eq!(p1, 6);
        let input = parse_input("testdata/18b.txt");
        let p1 = part1(&input, 10);
        assert_eq!(p1, 38);
    }
}
