type MaybeRange = Option<(isize, isize)>;

fn subtract((a, b): &(isize, isize), (c, d): &(isize, isize)) -> (MaybeRange, MaybeRange) {
    if d < a || c > b {
        // with a < b and c < d, there is no intersection
        (Some((*a, *b)), None)
    } else if c <= a && d >= b {
        return (None, None);
    } else {
        let cm1 = c - 1;
        let dp1 = d + 1;
        let i0 = if dp1 <= *b { Some((dp1, *b)) } else { None };
        let i1 = if cm1 >= *a { Some((*a, cm1)) } else { None };
        return (i0, i1);
    }
}

fn parse_input(input: &str) -> Vec<(isize, isize)> {
    std::fs::read_to_string(input)
        .unwrap_or_else(|_| panic!("Failed to read from file: {input}"))
        .lines()
        .filter_map(|l| l.split_once('-'))
        .map(|(s1, s2)| (s1.parse::<isize>().unwrap(), s2.parse::<isize>().unwrap()))
        .collect()
}

fn solution(input: &[(isize, isize)], n: isize) -> (isize, isize) {
    let mut res = vec![(0, n)];

    for s in input {
        let mut tmp: Vec<_> = Vec::new();
        for v in &res {
            match subtract(v, s) {
                (Some(v0), Some(v1)) => {
                    tmp.push(v0);
                    tmp.push(v1);
                }
                (Some(v0), None) => {
                    tmp.push(v0);
                }
                (None, Some(v0)) => {
                    tmp.push(v0);
                }
                (None, None) => {}
            }
        }
        res = tmp;
    }
    res.sort();
    let p1 = res.first().unwrap().0;
    let p2 = res.into_iter().map(|(f, t)| t - f + 1).sum();
    (p1, p2)
}

pub fn solve() -> (String, String) {
    let input = parse_input("input/20.txt");
    let (p1, p2) = solution(&input, 4294967295);
    (p1.to_string(), p2.to_string())
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn test_subtract() {
        let (r0, r1) = subtract(&(0, 10), &(3, 7));
        assert_eq!(r0, Some((8, 10)));
        assert_eq!(r1, Some((0, 2)));
        let (r0, r1) = subtract(&(2, 5), &(1, 3));
        assert_eq!(r0, Some((4, 5)));
        assert_eq!(r1, None);
        let (r0, r1) = subtract(&(3, 4), &(4, 7));
        assert_eq!(r1, Some((3, 3)));
        assert_eq!(r0, None)
    }
    #[test]
    fn test_part1() {
        let input = parse_input("testdata/20.txt");
        let (p1, p2) = solution(&input, 9);
        assert_eq!(p1, 3);
        assert_eq!(p2, 2);
    }
}
