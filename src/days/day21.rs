use itertools::Itertools;

#[derive(Debug)]
enum Operation {
    SwapPosition(usize, usize),
    SwapLetter(u8, u8),
    RotateLeft(usize),
    RotateRight(usize),
    ReverseBetween(usize, usize),
    RotateBased(u8),
    MovePosition(usize, usize),
}

fn parse_line(line: &str) -> Option<Operation> {
    if line.starts_with("swap position") {
        let mut iter = line.split_whitespace();
        iter.next();
        iter.next();
        let from = iter.next().unwrap().parse::<usize>().unwrap();
        iter.next();
        iter.next();
        let to = iter.next().unwrap().parse::<usize>().unwrap();
        Some(Operation::SwapPosition(from, to))
    } else if line.starts_with("swap letter") {
        let mut iter = line.split_whitespace();
        iter.next();
        iter.next();
        let from = iter.next().unwrap().chars().next().unwrap() as u8;
        iter.next();
        iter.next();
        let to = iter.next().unwrap().chars().next().unwrap() as u8;
        return Some(Operation::SwapLetter(from, to));
    } else if line.starts_with("rotate left") {
        let mut iter = line.split_whitespace();
        iter.next();
        iter.next();
        let steps = iter.next().unwrap().parse::<usize>().unwrap();
        return Some(Operation::RotateLeft(steps));
    } else if line.starts_with("rotate right") {
        let mut iter = line.split_whitespace();
        iter.next();
        iter.next();
        let steps = iter.next().unwrap().parse::<usize>().unwrap();
        return Some(Operation::RotateRight(steps));
    } else if line.starts_with("reverse positions") {
        let mut iter = line.split_whitespace();
        iter.next();
        iter.next();
        let a = iter.next().unwrap().parse::<usize>().unwrap();
        iter.next();
        let b = iter.next().unwrap().parse::<usize>().unwrap();
        return Some(Operation::ReverseBetween(a, b));
    } else if line.starts_with("rotate based") {
        let mut iter = line.split_whitespace();
        iter.next();
        iter.next();
        iter.next();
        iter.next();
        iter.next();
        iter.next();
        let a = iter.next().unwrap().chars().next().unwrap() as u8;
        return Some(Operation::RotateBased(a));
    } else if line.starts_with("move position") {
        let mut iter = line.split_whitespace();
        iter.next();
        iter.next();
        let a = iter.next().unwrap().parse::<usize>().unwrap();
        iter.next();
        iter.next();
        let b = iter.next().unwrap().parse::<usize>().unwrap();
        return Some(Operation::MovePosition(a, b));
    } else {
        None
    }
}

fn parse_input(input: &str) -> Vec<Operation> {
    std::fs::read_to_string(input)
        .unwrap_or_else(|_| panic!("Failed to read from file: {input}"))
        .lines()
        .filter_map(parse_line)
        .collect()
}

fn part1(input: &[Operation], start: &[u8]) -> String {
    let mut pass = start.to_vec();
    for op in input {
        match op {
            Operation::SwapPosition(a, b) => {
                pass.swap(*a, *b);
            }
            Operation::SwapLetter(a, b) => {
                let ia = pass.iter().position(|x| x == a).unwrap();
                let ib = pass.iter().position(|x| x == b).unwrap();
                pass.swap(ia, ib);
            }
            Operation::RotateLeft(s) => {
                pass.rotate_left(*s);
            }
            Operation::RotateRight(s) => {
                pass.rotate_right(*s);
            }
            Operation::ReverseBetween(a, b) => {
                let tmp = pass[*a..=*b].iter().copied().rev().collect::<Vec<u8>>();
                for (i, x) in tmp.into_iter().enumerate() {
                    pass[*a + i] = x;
                }
            }
            Operation::RotateBased(c) => {
                let mut i = pass.iter().position(|x| x == c).unwrap();
                if i >= 4 {
                    i += 1;
                }
                let l = pass.len();
                pass.rotate_right((i + 1).rem_euclid(l));
            }
            Operation::MovePosition(a, b) => {
                let x = pass.remove(*a);
                pass.insert(*b, x);
            }
        }
    }

    std::str::from_utf8(&pass).unwrap().to_string()
}

fn parse_input2(input: &str) -> String {
    std::fs::read_to_string(input)
        .unwrap_or_else(|_| panic!("Failed to read from file: {input}"))
        .trim()
        .to_string()
}

fn part2(input: &[Operation], target: &str) -> String {
    let base = "abcdefgh".bytes().collect::<Vec<u8>>();
    let perms = (0..target.len()).permutations(target.len());
    for p in perms {
        let pass = p.iter().map(|i| base[*i]).collect::<Vec<u8>>();
        let scrambled = part1(input, &pass);
        if scrambled == target {
            return std::str::from_utf8(&pass).unwrap().to_string();
        }
    }
    unreachable!();
}
// fbgdceah

fn solution(input_a: &[Operation], input_b: &str) -> (String, String) {
    let p1 = part1(input_a, &"abcdefgh".bytes().collect::<Vec<u8>>());
    let p2 = part2(input_a, input_b);

    (p1, p2)
}

pub fn solve() -> (String, String) {
    let input_a = parse_input("input/21.txt");
    let input_b = parse_input2("input/21b.txt");
    let (p1, p2) = solution(&input_a, &input_b);
    (p1, p2)
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn test_part1() {
        let input = parse_input("testdata/21.txt");
        let p1 = part1(&input, &"abcde".bytes().collect::<Vec<u8>>());
        assert_eq!(p1, "decab");

        // let (p1, p2) = solution(&input);
        // assert_eq!(p2, 0);
        // assert_eq!(p1, 0);
    }
}
