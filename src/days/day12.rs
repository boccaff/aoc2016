use crate::assembunny_interpreter;
use crate::parse_assembunny_program;
use crate::Instr;

fn solution(input: &[Instr]) -> (isize, isize) {
    let (p1, _) = assembunny_interpreter(input, &[0, 0, 0, 0], 1_usize);
    let (p2, _) = assembunny_interpreter(input, &[0, 0, 1, 0], 1_usize);
    (p1[0_usize], p2[0_usize])
}

pub fn solve() -> (String, String) {
    let input = parse_assembunny_program("input/12.txt");
    let (p1, p2) = solution(&input);
    (p1.to_string(), p2.to_string())
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn test_part1() {
        let input = parse_assembunny_program("testdata/12.txt");
        let (p1, _) = solution(&input);
        assert_eq!(p1, 42);
    }
}
