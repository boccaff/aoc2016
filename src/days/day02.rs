use hashbrown::HashMap;
use pathfinding::matrix::directions::DIRECTIONS_4;
use pathfinding::matrix::Matrix;
use pathfinding::utils::move_in_direction;

fn parse_input(input: &str) -> Vec<Vec<u8>> {
    std::fs::read_to_string(input)
        .unwrap()
        .lines()
        .map(|l| l.bytes().collect())
        .collect()
}

fn solution(input: &[Vec<u8>]) -> (isize, String) {
    let moves: HashMap<u8, (isize, isize)> = [b'R', b'D', b'L', b'U']
        .into_iter()
        .zip(DIRECTIONS_4)
        .collect();

    let keypad2 = Matrix::from_vec(
        5,
        5,
        vec![
            '0', '0', '1', '0', '0', '0', '2', '3', '4', '0', '5', '6', '7', '8', '9', '0', 'A',
            'B', 'C', '0', '0', '0', 'D', '0', '0',
        ],
    )
    .unwrap();

    let mut p1 = (1, 1);
    let mut p2 = (2, 0);
    let mut ps1 = Vec::new();
    let mut ps2 = Vec::new();

    for mvs in input {
        for mv in mvs {
            p1 = move_in_direction(p1, moves[mv], (3, 3)).unwrap_or(p1);
            let new_p2 = move_in_direction(p2, moves[mv], (5, 5)).unwrap_or(p2);
            if keypad2[new_p2] != '0' {
                p2 = new_p2;
            }
        }
        ps1.push(p1);
        ps2.push(keypad2[p2]);
    }

    let p1: usize = ps1
        .into_iter()
        .map(|(i, j)| j + 1 + i * 3)
        .rev()
        .enumerate()
        .map(|(i, x)| x * 10_usize.pow(i as u32))
        .sum();

    let p2: String = ps2.into_iter().collect();
    (p1 as isize, p2)
}

pub fn solve() -> (String, String) {
    let input = parse_input("input/02.txt");
    let (p1, p2) = solution(&input);
    (p1.to_string(), p2.to_string())
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn test_part1() {
        let input = parse_input("testdata/02.txt");
        let (p1, _) = solution(&input);
        assert_eq!(p1, 1985);
    }

    #[test]
    fn test_part2() {
        let input = parse_input("testdata/02.txt");
        let (_, p2) = solution(&input);
        assert_eq!(p2, "5DB3".to_string());
    }
}
