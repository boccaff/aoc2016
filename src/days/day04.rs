use scan_fmt::scan_fmt;

type Room = (String, isize, String);
fn count_chars(s: &str) -> [isize; 26] {
    let mut arr = [0; 26];
    s.as_bytes().iter().for_each(|c| {
        if *c != b'-' {
            arr[(c - b'a') as usize] += 1;
        }
    });
    arr
}

fn parse_line(line: &str) -> Room {
    match scan_fmt!(line, "{[a-z-]}{d}[{}]", String, isize, String) {
        Ok((room, id, checksum)) => (room, id, checksum),
        Err(e) => panic!("Invalid element on list: '{}', with error: {:}", line, e),
    }
}

fn parse_input(input: &str) -> Vec<Room> {
    std::fs::read_to_string(input)
        .unwrap()
        .lines()
        .map(parse_line)
        .collect()
}

fn calc_checksum(name: &str) -> String {
    let mut char_count = count_chars(name)
        .into_iter()
        .enumerate()
        .map(|(i, x)| (x, -(i as isize)))
        .collect::<Vec<(isize, isize)>>();

    char_count.sort();

    std::str::from_utf8(
        &char_count
            .into_iter()
            .rev()
            .take(5)
            .map(|(_, i)| (-i as u8) + b'a')
            .collect::<Vec<u8>>(),
    )
    .unwrap()
    .to_string()
}

fn decrypt(s: &str, i: isize) -> String {
    std::str::from_utf8(
        &s.as_bytes()
            .iter()
            .map(|c| {
                if *c != b'-' {
                    (c - (b'a') + (i.rem_euclid(26)) as u8).rem_euclid(26) + b'a'
                } else {
                    *c
                }
            })
            .collect::<Vec<u8>>(),
    )
    .unwrap()
    .to_string()
}

fn solution(input: &[Room]) -> (isize, isize) {
    let mut p1 = 0;
    let mut p2 = -1;

    for (name, id, cs) in input {
        if cs == &calc_checksum(name) {
            p1 += id;
        }
        if decrypt(name, *id).contains("northpole") {
            p2 = *id;
        }
    }

    (p1, p2)
}

pub fn solve() -> (String, String) {
    let input = parse_input("input/04.txt");
    let (p1, p2) = solution(&input);
    (p1.to_string(), p2.to_string())
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn test_part1() {
        let input = parse_input("testdata/04.txt");
        let (p1, _) = solution(&input);
        assert_eq!(p1, 1514);
    }
}
