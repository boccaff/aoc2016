fn parse_input(input: &str) -> Vec<u8> {
    std::fs::read_to_string(input)
        .unwrap_or_else(|_| panic!("Failed to read from file: {input}"))
        .trim()
        .bytes()
        .map(|c| c - b'0')
        .collect()
}

fn reverse(input: &[u8]) -> Vec<u8> {
    input
        .iter()
        .rev()
        .map(|&x| if x == 1 { 0 } else { 1 })
        .collect()
}

fn generate(input: &[u8]) -> Vec<u8> {
    let mut arr = Vec::with_capacity(2 * input.len() + 1);

    arr.extend_from_slice(input);
    arr.push(0);
    arr.extend_from_slice(&reverse(input));
    arr
}

fn checksum(input: &[u8]) -> Vec<u8> {
    if input.len() < 2 || input.len() & 1 == 1 {
        panic!("Asked checksum for ({}): {input:?}", input.len())
    }
    input
        .chunks(2)
        .map(|x| if x[0] == x[1] { 1 } else { 0 })
        .collect()
}

fn randomdata(input: &[u8], disk_size: usize) -> String {
    let mut arr: Vec<u8> = input.to_vec();

    while arr.len() < disk_size {
        arr = generate(&arr);
    }
    arr = arr[0..disk_size].to_vec();

    let mut cs = checksum(&arr);
    while cs.len() & 1 == 0 {
        cs = checksum(&cs);
    }

    let p1 = std::str::from_utf8(&cs.iter().map(|x| x + b'0').collect::<Vec<u8>>())
        .unwrap()
        .to_string();
    p1
}

pub fn solve() -> (String, String) {
    let input = parse_input("input/16.txt");
    let p1 = randomdata(&input, 272);
    let p2 = randomdata(&input, 35651584);
    (p1, p2)
}

#[cfg(test)]
mod test {

    use super::*;

    #[test]
    fn test_part1() {
        let input = parse_input("testdata/16.txt");
        let p1 = randomdata(&input, 20);
        assert_eq!(p1, "01100".to_string());
    }
}
