use pathfinding::directed::dijkstra::dijkstra;

fn element_to_u8(elem: &str) -> usize {
    for (pat, ret) in [
        ("curium", 0),
        ("plutonium", 1),
        ("ruthenium", 2),
        ("strontium", 3),
        ("thulium", 4),
        ("hydrogen", 0),
        ("lithium", 1),
    ] {
        if elem.contains(pat) {
            return ret;
        }
    }
    panic!("'{elem}' does not contain a known element!")
}

fn parse_line(line: &str) -> Vec<(usize, bool)> {
    let (_, l) = line.split_once("contains").unwrap();
    if l == " nothing relevant." {
        Vec::new()
    } else {
        l.split(", ")
            .map(|x| (element_to_u8(x), x.contains("-compatible")))
            .collect()
    }
}

pub fn parse_input(input: &str) -> ([usize; 8], [usize; 8]) {
    let mut g = [0_usize; 8];
    let mut m = [0_usize; 8];

    std::fs::read_to_string(input)
        .unwrap()
        .lines()
        .map(parse_line)
        .filter(|x| !x.is_empty())
        .enumerate()
        .for_each(|(i, pieces)| {
            pieces.into_iter().for_each(|(p, is_microchip)| {
                if is_microchip {
                    m[p] = i + 1;
                } else {
                    g[p] = i + 1;
                }
            })
        });
    (g, m)
}

fn is_safe(g: [usize; 8], m: [usize; 8]) -> bool {
    // unsafe if a generator occupy the same space with a uncoupled microchip
    for i in 0..8 {
        if g[i] > 0 {
            for j in 0..8 {
                if i != j && m[j] != g[j] && m[j] == g[i] {
                    return false;
                }
            }
        }
    }
    true
}

fn sucessors((f, g, m): &(usize, [usize; 8], [usize; 8])) -> Vec<(usize, [usize; 8], [usize; 8])> {
    let mut s = Vec::new();
    let f = *f;
    let g = *g;
    let m = *m;

    // track the movements of generators and microchips
    let mut gmv = false;
    let mut mmv = false;

    // move things up!
    if f < 4 {
        // move one of each up
        for i in 0..8 {
            if (g[i] > 0) && (g[i] == f) && (m[i] == f) {
                let mut g2 = g;
                g2[i] += 1;
                let mut m2 = m;
                m2[i] += 1;
                s.push((f + 1, g2, m2));
                break;
            }
        }
        // try to move generators up
        'a: for i in 0..8 {
            if g[i] > 0 && g[i] == f {
                for j in 0..i {
                    if g[j] > 0 && g[j] == f {
                        let mut g2 = g;
                        g2[i] += 1;
                        g2[j] += 1;
                        if is_safe(g2, m) {
                            s.push((f + 1, g2, m));
                            gmv = true;
                            break 'a;
                        }
                    }
                }
            }
        }
        // above will not push with a single generator on the floor
        if !gmv {
            for i in 0..8 {
                if g[i] > 0 && g[i] == f {
                    let mut g2 = g;
                    g2[i] += 1;
                    if is_safe(g2, m) {
                        s.push((f + 1, g2, m));
                        break;
                    }
                }
            }
        }
        // try to move microchips up
        'a: for i in 0..8 {
            if m[i] > 0 && m[i] == f {
                for j in 0..i {
                    if m[j] > 0 && m[j] == f {
                        let mut m2 = m;
                        m2[i] += 1;
                        m2[j] += 1;
                        if is_safe(g, m2) {
                            s.push((f + 1, g, m2));
                            mmv = true;
                            break 'a;
                        }
                    }
                }
            }
        }
        // above will not push when only one microchip is in the floor
        if !mmv {
            for i in 0..8 {
                if m[i] > 0 && m[i] == f {
                    let mut m2 = m;
                    m2[i] += 1;
                    if is_safe(g, m2) {
                        s.push((f + 1, g, m2));
                        break;
                    }
                }
            }
        }
    }

    // try to move things down
    if f > 1 && !gmv && !mmv {
        for i in 0..8 {
            if g[i] > 0 && g[i] == f {
                let mut g2 = g;
                g2[i] -= 1;
                if is_safe(g2, m) {
                    s.push((f - 1, g2, m));
                    break;
                }
            }
        }

        for i in 0..8 {
            if m[i] > 0 && m[i] == f {
                let mut m2 = m;
                m2[i] -= 1;
                if is_safe(g, m2) {
                    s.push((f - 1, g, m2));
                    break;
                }
            }
        }
    }

    s
}

fn find_goal((_, g, m): (usize, [usize; 8], [usize; 8])) -> (usize, [usize; 8], [usize; 8]) {
    let mut g = g;
    let mut m = m;

    for i in 0..8 {
        if g[i] != 0 {
            g[i] = 4;
        }
        if m[i] != 0 {
            m[i] = 4;
        }
    }
    (4, g, m)
}

pub fn solution((g, m): &([usize; 8], [usize; 8])) -> isize {
    let goal = find_goal((0, *g, *m));
    let result = dijkstra(
        &(1_usize, *g, *m),
        |p| sucessors(p).into_iter().map(|x| (x, 1)).collect::<Vec<_>>(),
        |p| *p == goal,
    );
    let (_, p1) = result.unwrap();
    p1
}

pub fn solve() -> (String, String) {
    let mut input = parse_input("input/11.txt");
    let p1 = solution(&input);
    input.1[5] = 1;
    input.1[6] = 1;
    input.0[5] = 1;
    input.0[6] = 1;
    let p2 = solution(&input);
    (p1.to_string(), p2.to_string())
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn test_solution() {
        let input = parse_input("testdata/11.txt");
        let p1 = solution(&input);
        assert_eq!(p1, 11);
    }
}
