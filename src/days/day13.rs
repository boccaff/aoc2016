use pathfinding::prelude::dijkstra;
use pathfinding::prelude::dijkstra_all;

fn parse_input(input: &str) -> usize {
    std::fs::read_to_string(input)
        .unwrap_or_else(|_| panic!("Failed to read from file: {input}"))
        .trim()
        .parse::<usize>()
        .unwrap()
}

fn is_empty_space(x: usize, y: usize, c: usize) -> bool {
    let mut s = x * x + 3 * x + 2 * x * y + y + y * y;
    s += c;
    let mut n = 0;
    while s > 0 {
        if (s & 1) == 1 {
            n += 1;
        }
        s >>= 1;
    }
    (n & 1) == 0
}

fn sucessors((x, y): (usize, usize), c: usize) -> Vec<((usize, usize), usize)> {
    let arr: [(isize, isize); 4] = [(0, 1), (0, -1), (1, 0), (-1, 0)];
    arr.into_iter()
        .map(|(dx, dy)| {
            if dx < 0 {
                (x.saturating_sub(1), y)
            } else if dy < 0 {
                (x, y.saturating_sub(1))
            } else {
                (x + dx as usize, y + dy as usize)
            }
        })
        .filter(|&p| p != (x, y))
        .filter(|&(x, y)| is_empty_space(x, y, c))
        .map(|p| (p, 1))
        .collect()
}

fn find_path(input: &usize, goal: &(usize, usize)) -> usize {
    let (_, cost) = dijkstra(&(1, 1), |p| sucessors(*p, *input), |p| p == goal).unwrap();
    cost
}

fn find_reachable(input: &usize, limit: usize) -> usize {
    let reachables = dijkstra_all(&(1, 1), |p| sucessors(*p, *input));
    reachables
        .into_values()
        .filter(|&(_, c)| c <= limit)
        .count()
}

fn solution(input: &usize) -> (usize, usize) {
    let p1 = find_path(input, &(31, 39));
    let p2 = find_reachable(input, 51);
    (p1, p2)
}

pub fn solve() -> (String, String) {
    let input = parse_input("input/13.txt");
    let (p1, p2) = solution(&input);
    (p1.to_string(), p2.to_string())
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn test_solution() {
        let input = parse_input("testdata/13.txt");
        let p1 = find_path(&input, &(7, 4));
        assert_eq!(p1, 11);
    }
}
