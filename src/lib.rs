use std::ops::Sub;

pub mod days;

#[derive(Debug, Copy, Clone)]
pub enum Value {
    Reg(u8),
    Int(isize),
}

#[derive(Debug, Copy, Clone)]
pub enum Instr {
    Cpy(Value, Value),
    Inc(Value),
    Dec(Value),
    Jnz(Value, Value),
    Tgl(Value),
    Out(Value),
    Add(Value, Value),
    Mul(Value, Value),
    Nop,
}

fn parse_assembunny_value(value: &str) -> Value {
    if value.chars().all(|c| c.is_alphabetic()) {
        Value::Reg(value.chars().next().unwrap() as u8 - b'a')
    } else {
        Value::Int(value.parse::<isize>().unwrap())
    }
}

pub fn parse_assembunny_line(line: &str) -> Instr {
    let (left, right) = line.split_once(' ').unwrap();
    match left {
        "cpy" => {
            let (f, t) = right.split_once(' ').unwrap();
            Instr::Cpy(parse_assembunny_value(f), parse_assembunny_value(t))
        }
        "inc" => Instr::Inc(Value::Reg(right.chars().next().unwrap() as u8 - b'a')),
        "dec" => Instr::Dec(Value::Reg(right.chars().next().unwrap() as u8 - b'a')),
        "jnz" => {
            let (f, t) = right.split_once(' ').unwrap();
            Instr::Jnz(parse_assembunny_value(f), parse_assembunny_value(t))
        }
        "tgl" => Instr::Tgl(Value::Reg(right.chars().next().unwrap() as u8 - b'a')),
        "out" => Instr::Out(parse_assembunny_value(right)),
        _ => panic!("Received instruction: {left}"),
    }
}

pub fn parse_assembunny_program(input: &str) -> Vec<Instr> {
    std::fs::read_to_string(input)
        .unwrap_or_else(|_| panic!("Failed to read from file: {input}"))
        .lines()
        .map(parse_assembunny_line)
        .collect()
}

pub fn assembunny_interpreter(
    input: &[Instr],
    arr: &[isize; 4],
    l: usize,
) -> ([isize; 4], Vec<isize>) {
    let mut i = 0;
    let mut arr = *arr;
    let mut instr: Vec<(bool, Instr)> = input.iter().map(|&i| (true, i)).collect();
    let mut out = Vec::with_capacity(l);

    loop {
        if i >= instr.len() {
            break;
        }

        if instr[i].0 {
            match instr[i].1 {
                Instr::Cpy(v1, v2) => match (v1, v2) {
                    (Value::Reg(x1), Value::Reg(x2)) => {
                        arr[x2 as usize] = arr[x1 as usize];
                    }
                    (Value::Int(x1), Value::Reg(x2)) => {
                        arr[x2 as usize] = x1;
                    }
                    (Value::Reg(_), Value::Int(_)) => {
                        // continue;
                        // panic!()
                    }
                    (Value::Int(_), Value::Int(_)) => {
                        // continue;
                        // panic!()
                    }
                },
                Instr::Inc(v) => match v {
                    Value::Reg(x) => {
                        arr[x as usize] += 1;
                    }
                    Value::Int(_) => {
                        // continue;
                        // panic!()
                    }
                },
                Instr::Dec(v) => match v {
                    Value::Reg(x) => {
                        arr[x as usize] -= 1;
                    }
                    Value::Int(_) => {
                        // continue;
                        // panic!()
                    }
                },
                Instr::Jnz(v1, v2) => match (v1, v2) {
                    (Value::Reg(x1), Value::Int(x2)) => {
                        if arr[x1 as usize] != 0 {
                            i = if x2 > 0 {
                                i + x2 as usize
                            } else {
                                i.sub(-x2 as usize)
                            };
                            continue;
                        }
                    }
                    (Value::Int(x1), Value::Int(x2)) => {
                        if x1 != 0 {
                            i += x2 as usize;
                            continue;
                        }
                    }
                    (Value::Int(x1), Value::Reg(x2)) => {
                        if x1 != 0 {
                            let x2 = arr[x2 as usize];
                            i = if x2 > 0 {
                                i + x2 as usize
                            } else {
                                i.sub(-x2 as usize)
                            };
                            continue;
                        }
                        // continue;
                        // panic!()
                    }
                    (Value::Reg(_), Value::Reg(_)) => {
                        // continue;
                        // panic!()
                    }
                },
                Instr::Add(v1, v2) => match (v1, v2) {
                    // add v1 to v2 (v2 = v2 + v1)
                    (Value::Reg(_), Value::Int(_)) => {
                        // does not make sense
                    }
                    (Value::Int(_), Value::Int(_)) => {
                        // does not make sense
                    }
                    (Value::Int(x1), Value::Reg(x2)) => {
                        arr[x2 as usize] += x1;
                    }
                    (Value::Reg(x1), Value::Reg(x2)) => {
                        arr[x2 as usize] += arr[x1 as usize];
                    }
                },
                Instr::Mul(v1, v2) => match (v1, v2) {
                    // mul v1 into v2 (v2 *= v1)
                    (Value::Reg(_), Value::Int(_)) => {
                        // does not make sense
                    }
                    (Value::Int(_), Value::Int(_)) => {
                        // does not make sense
                    }
                    (Value::Int(x1), Value::Reg(x2)) => {
                        arr[x2 as usize] *= x1;
                    }
                    (Value::Reg(x1), Value::Reg(x2)) => {
                        arr[x2 as usize] *= arr[x1 as usize];
                    }
                },
                Instr::Out(v) => match v {
                    Value::Reg(x) => {
                        out.push(arr[x as usize]);
                        if out.len() == l {
                            println!("HERE");
                            break;
                        }
                    }
                    Value::Int(_) => {}
                },
                Instr::Tgl(v) => {
                    // println!("\n");
                    // println!("in: {instr:?}");
                    let x = match v {
                        Value::Reg(r) => arr[r as usize] as usize,
                        Value::Int(i) => i as usize,
                    };
                    if i + x < instr.len() {
                        match instr[i + x].1 {
                            Instr::Out(_) => {}
                            Instr::Nop => {}
                            Instr::Inc(v1) => instr[i + x].1 = Instr::Dec(v1),
                            Instr::Dec(v1) => instr[i + x].1 = Instr::Inc(v1),
                            Instr::Jnz(v1, v2) => instr[i + x].1 = Instr::Cpy(v1, v2),
                            Instr::Cpy(v1, v2) => instr[i + x].1 = Instr::Jnz(v1, v2),
                            Instr::Add(v1, v2) => instr[i + x].1 = Instr::Jnz(v1, v2),
                            Instr::Mul(v1, v2) => instr[i + x].1 = Instr::Jnz(v1, v2),
                            Instr::Tgl(v1) => {
                                instr[i + x].1 = Instr::Inc(v1);
                                if x == 0 {
                                    instr[i + x].0 = false;
                                }
                            }
                        };
                        // println!("out: {instr:?}");
                    }
                }
                Instr::Nop => {}
            }
        } else {
            instr[i].0 = true;
        }
        i += 1;
    }
    (arr, out)
}

pub fn max_min_x(slice: &[(isize, isize)]) -> (isize, isize) {
    slice.iter().fold((isize::MIN, isize::MAX), |acc, (x, _)| {
        (acc.0.max(*x), acc.1.min(*x))
    })
}

pub fn max_min_y(slice: &[(isize, isize)]) -> (isize, isize) {
    slice.iter().fold((isize::MIN, isize::MAX), |acc, (_, y)| {
        (acc.0.max(*y), acc.1.min(*y))
    })
}

// TODO: write tests for this
pub fn str_to_usize(s: &str) -> usize {
    s.as_bytes()
        .iter()
        .rev()
        .enumerate()
        .map(|(i, c)| (c - b'a') as usize * 26_usize.pow(i as u32))
        .sum()
}

pub fn signed_int_vec(s: &str) -> Vec<isize> {
    let mut v: Vec<isize> = Vec::with_capacity(s.len() / 2_usize);
    let mut i = 0_u32;
    let mut n = 0_isize;
    let mut is_neg = false;
    let mut is_number = false;
    s.bytes().rev().for_each(|c| {
        if c.is_ascii_digit() {
            is_number = true;
            n += (c - b'0') as isize * 10_isize.pow(i);
            i += 1;
        } else if c == b'-' {
            is_neg = true;
        } else if is_number {
            if is_neg {
                v.push(-n);
                is_neg = false;
                is_number = false;
            } else {
                v.push(n);
                is_neg = false;
                is_number = false;
            }
            n = 0;
            i = 0;
        } else {
            n = 0;
            i = 0;
        }
    });
    if n != 0 {
        if is_neg {
            v.push(-n);
        } else {
            v.push(n);
        }
    }
    v.iter().rev().copied().collect()
}

pub fn unsigned_int_vec(s: &str) -> Vec<isize> {
    let mut v: Vec<isize> = Vec::with_capacity(s.len() / 2_usize);
    let mut i = 0_u32;
    let mut n = 0_isize;
    let mut is_number = false;
    s.bytes().rev().for_each(|c| {
        if c.is_ascii_digit() {
            is_number = true;
            n += (c - b'0') as isize * 10_isize.pow(i);
            i += 1;
        } else if is_number {
            v.push(n);
            is_number = false;
            n = 0;
            i = 0;
        } else {
            n = 0;
            i = 0;
        }
    });
    if n != 0 {
        v.push(n);
    }
    v.iter().rev().copied().collect()
}

pub fn u8neighbors_tuple((x, y): (usize, usize)) -> Vec<(usize, usize)> {
    let mut res = Vec::from([
        (x + 1_usize, y),
        (x, y + 1_usize),
        (x + 1_usize, y + 1_usize),
    ]);

    //res.extend(Vec::from([
    //]))
    if x > 0 && y > 0 {
        res.push((x + 1_usize, y - 1_usize));
        res.push((x, y - 1_usize));
        res.push((x - 1_usize, y - 1_usize));
        res.push((x - 1_usize, y));
        res.push((x - 1_usize, y + 1_usize));
    } else if y > 0 {
        res.push((x + 1_usize, y - 1_usize));
        res.push((x, y - 1_usize));
    } else if x > 0 {
        res.push((x - 1_usize, y));
        res.push((x - 1_usize, y + 1_usize));
    }
    res
}

pub fn u4neighbors_tuple((x, y): (usize, usize)) -> Vec<(usize, usize)> {
    let mut res = Vec::from([(x + 1_usize, y), (x, y + 1_usize)]);

    //res.extend(Vec::from([
    //]))
    if x > 0 && y > 0 {
        res.push((x, y - 1_usize));
        res.push((x - 1_usize, y));
    } else if y > 0 {
        res.push((x, y - 1_usize));
    } else if x > 0 {
        res.push((x - 1_usize, y));
    }
    res
}

// fn neighbors([x, y]: [u16; 2], [lx, ly]: [u16; 2]) -> Vec<[u16; 2]> {
//     let arr: [(isize, isize); 4] = [(0, 1), (0, -1), (1, 0), (-1, 0)];
//     arr.into_iter()
//         .map(|(dx, dy)| {
//             if dx < 0 {
//                 [x.saturating_sub(1), y]
//             } else if dy < 0 {
//                 [x, y.saturating_sub(1)]
//             } else {
//                 [lx.min(x + dx as u16), ly.min(y + dy as u16)]
//             }
//         })
//         .filter(|&p| p != [x, y])
//         .collect()
// }

pub fn neighbors4((x, y): (isize, isize)) -> Vec<(isize, isize)> {
    [(0, 1), (1, 0), (0, -1), (-1, 0)]
        .iter()
        .map(|(dx, dy)| (x + dx, y + dy))
        .collect::<Vec<_>>()
}
// add a cycle detection algorithm
// parse list of ints without rellying on .parse()

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn test_unsigned_int_vec() {
        assert_eq!(
            unsigned_int_vec("10,   20, 30"),
            vec![10_isize, 20_isize, 30_isize]
        );
        assert_eq!(unsigned_int_vec("1 2 3"), vec![1_isize, 2_isize, 3_isize]);
        assert_eq!(
            unsigned_int_vec("15|24|33"),
            vec![15_isize, 24_isize, 33_isize]
        );
        assert_eq!(
            unsigned_int_vec("1,1,1,1,1,1,1"),
            vec![1_isize, 1_isize, 1_isize, 1_isize, 1_isize, 1_isize, 1_isize]
        );
    }

    #[test]
    fn test_signed_int_vec() {
        assert_eq!(
            signed_int_vec("10,  -20,    30"),
            vec![10_isize, -20_isize, 30_isize]
        );
        assert_eq!(signed_int_vec("-1 2 3"), vec![-1_isize, 2_isize, 3_isize]);
        assert_eq!(
            signed_int_vec("-15|-24|-33"),
            vec![-15_isize, -24_isize, -33_isize]
        );
        assert_eq!(
            signed_int_vec("1,1,1,1,1,1,1"),
            vec![1_isize, 1_isize, 1_isize, 1_isize, 1_isize, 1_isize, 1_isize]
        );
    }
}
