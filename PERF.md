# TL; DR

While I've started with a goal of reaching 100ms runtime for all days, I was only able reach ~900ms, being restricted by the time required to run searches over Md5 hashes. After completing all days, the total runtime of running all days in parallel in almost 9 seconds, including day14 using `rayon` to search through md5 hashes in batches. The slow days that did not involve searching over md5 hashes were significantly improved, specially days 11 and 23. But after speeding them, the runtime dominated by md5 hashes calculation.

| Day | Baseline | End result |
|-----|----------|------------|
|  11 |   8.16s  |   23.65ms  | 
|  23 |   5.95s  |   65.13us  | 
|  05 |   4.10s  |  443.45ms  |
|  18 | 693.60ms |  302.51ms  |


Adding parallelization to day05 benefited only untill ~10 workers were used to search hashes and without much to do on day14, solving those two days are the bottleneck in speed. After that, I had no motivation to continue to optimize further. 10x results were achieved, and the initial uninformed expectations were unrealistic due to the hashing effort.


# Lets optimize it!

The 3 slowest day clock at more than 4 secods (days 11, 23 and 5), followed by 5 problems between ~ 100ms to 1s (18, 14, 22, 17 and 15), with the remainder problems taking less than 80ms. Considering that 100ms have an "almost instantaneous feel", this can be a nice target. An intermediate target could be 500 ms, and under 1s for everything should be fast to reach, depending on those MD5 hashes being viable. 

```
Day 11: 8.77s
Day 23: 4.98s
Day 05:  4.50s
Day 18: 1.01s
Day 14: 622.65ms
Day 22: 213.81ms
Day 17: 145.79ms
Day 15: 110.12ms
Day 16: 76.91ms
Day 12: 51.54ms
Day 21: 19.19ms
Day 24: 13.75ms
Day 08: 11.19ms
Day 01: 11.50ms
Day 02: 10.26ms
Day 10: 9.31ms
Day 09: 9.03ms
Day 04: 7.61ms
Day 20: 5.48ms
Day 07: 5.35ms
Day 13: 3.91ms
Day 03: 1.79ms
Day 06: 1.38ms
Day 19: 904.24µs
Day 25: 674.26µs
```


## Day 11

This problem require us to move pairs of items accross floors following some rules about which items can be transported or left together. The solution is using a `[u8; 8]` array to track the state, using a bitmask to track the items in each floor, where the first four positions track one type of the pair (generator) and the last four positions tracking the other type (microchip). The solution is a graph transversal using Dijkstra, with most of the logic encoded in the function that generate the sucessors. There are two suspects here, Vec allocations and generating a lot of states. The [reddit thread](https://www.reddit.com/r/adventofcode/comments/5hoia9/2016_day_11_solutions/#s) mentions that state prunning helps a lot, with a rust solution under 100ms for both parts. For the `Vec` part, even though the state arrays have `Copy`, generating the sucessors require the collection of three vectors of arrays, and this could be slow. One alternative here could be implementing a solution using A*, as states with more items close to the 4th floor will require less steps.

First benchmark results shows (btw, perf wrote 12 GB here and I was having freezes on neovim):

```
parse_input             time:   [4.2770 µs 4.2814 µs 4.2866 µs]
solution (p1)           time:   [116.10 ms 116.35 ms 116.63 ms]
solution (p2)           time:   [8.0524 s 8.0750 s 8.0982 s]
```

So we have the increased number of items exploding the size of the state space in part2. As the initial solution builds all the possible movements for 1 or 2 items, moving only microchips, or only generators, and the cartesian product of moving one of each, we have this factorial explosion of possible states. Analyzing the flamegraph output, we can see that most of the time is spent on `sucessors`, dominated by `collect`.

After a first attempt that incresed the runtime by 50%, removing all iterators and having explicit for loops filtering unsafe states before pushing yielded marginal gains. Following up on [this](https://www.reddit.com/r/adventofcode/comments/5hoia9/2016_day_11_solutions/db1v1ws/) comment, I've started breaking after finding candidate for each type of movement (move a pair, move only a generator and only a microchip), resulting in a ~50% improvement over the baseline.

```
solution (p1)           time:   [47.165 ms 47.328 ms 47.500 ms]
solution (p2)           time:   [4.2684 s 4.2780 s 4.2885 s]
```

The last optimization mentioned is about having invariance over the pairs of microchips (hydrogen pair on (0, 1) and lythium pair at (0, 2) is the same as hydrogen at (0, 2) and lythium at (0, 1)), and prunning states that are equivalent. As this is not something that I can easily modify into the `pathfinding::prelude::dijkstra` function, I would need a representation that is invariant the type of pair, or implement a struct with such `Eq` trait. While this may be done, there was the A* implementation, which should be fairly straightforward here. That resulted in:

```
solution (p1)           time:   [27.281 µs 27.348 µs 27.415 µs]
solution (p2)           time:   [221.06 ms 221.90 ms 222.80 ms]
```

While other days are taking most of the time, I am curious about how much could be gained by using a single u64 or two u32 to represent the positions. It was way worse. TODO: find out

It got me wandering if I could find a better representation, as it looks that I am always struggling with encoding/decoding the state from compact representations. I went with two arrays for microchips and generators, where each position is an element that will have the floor stored or zero if the element is not present. Also, thinking more about the safety of moves, the main issue is moving generators into floors that have unpowered microchips, or moving a unmatched pair of components. So, instead of `is_safe` I can look just for non-zero microchips without a match in the generators array. This resulted in 

```
parse_input             time:   [4.6627 µs 4.6812 µs 4.7007 µs]
solution (p1)           time:   [1.4544 ms 1.4565 ms 1.4587 ms]
solution (p2)           time:   [22.208 ms 22.337 ms 22.472 ms]
```

**Lesson of the day**: premature optimization of ram occupied lead to wasting time on writting the state sucession code.

## Day 23

Day 23 is the implementantion of a VM on top of Day 12. Looking into the "program" running, one can see the 'hot loop' on 5 instructions that implement multiplication with 'inc x', where the register x is increased one by one, and this takes a lot of time, as the program is is calculating a factorial and adding it to the product of two integers close to 100. The main issue is the factorial on part 2, where we go from `7!` (5040) to `12!` (479001600), while the constant being added is 6853. While part 1 runs in micro seconds, part 2 runs in 5+ seconds. Benchmarking the baseline yields:

```
parse_input             time:   [4.9133 µs 4.9405 µs 4.9730 µs]
part 1                  time:   [96.686 µs 97.337 µs 98.119 µs]
part 2                  time:   [5.9380 s 5.9486 s 5.9593 s]
```
Two ways of solving this, creating a different set of operations to make it faster (implenting something like 'add a b' or 'mul a b') or straight up calculating the factorial + constant (this ended up being equal to this [answer](https://www.reddit.com/r/adventofcode/comments/5jvbzt/2016_day_23_solutions/dbjdmj3/)). Using the "will be fun" criteria, let's create the following instructions:
- `add a b` set register b to a + b
- `mul a b` set register b to a * b
- `nop` skip instruction

While solving 2016, I did a fairly bad job of reusing code, importing things from `day12` into days 23 and 25, and creating a different `interpreter` for each day. So it was time to step back and move the shared code from the multiple `days` into `lib`. After doing that, and creating the three instructions above, I've started replacing the set of five instructions:

```
cpy b c
inc a
dec c
jnz c -2
dec d
```

with:

```
mul b d
add d a
nop
nop
nop
```
The code entered an infinite loop, as the next instruction would be `jnz d -5`, which is jump 5 instructions back if `d` is not zero. The end of the block implied `c` and `d` equal to `0`, so the next change would be:

```
mul b d
add d a
cpy 0 d
cpy 0 c
nop
```

After struggling a bit with the code running forever, I've realized that I was changing instructions 6-10, but those were the 'lines' in the input, so I needed to actually change the instructions 5-9 (but I still prefer 0 based indexing). After that, the code run in:

```
parse_input             time:   [4.7427 µs 4.7537 µs 4.7650 µs]
solution                time:   [59.181 µs 59.387 µs 59.635 µs]
```

6 s / 60 us, an 5 orders or magnitude improvement. So nice! With this optimization the days above 100 ms are:

```
Day 05	4.38s
Day 18	937.42ms
Day 14	537.49ms
Day 22	217.76ms
Day 17	136.22ms
Day 15	103.45ms
```

The thing in the thread that also caught my attention was this [solution](https://www.reddit.com/r/adventofcode/comments/5hus40/2016_day_12_solutions/db3eoyo/), with the actual script being moved to [here](https://github.com/bpeel/advent2016/blob/master/2016/day12.sh).
Using sed to convert the source to a C program with inline assembler. I did the same thing last year too.

This have a 'dark arts / forgotten secrets' feeling.

## Day 05

Day 05 requires processing 25M MD5 hashes, and along with days 14 and 17 that require hashing, may be the bottleneck to speed up this whole project below 100ms. For day 14, I've already used an batch calculation that leverages all cores to pre-calculate hashes in a way that have sped up the problem. Another alternative is to have some threads calculating hashes while some other worker check their properties. The flamegraph here shows that we have most of the time serializing into hex and calculating hashes.

```
parse_input             time:   [3.5099 µs 3.5182 µs 3.5277 µs]
solution                time:   [4.0891 s 4.0979 s 4.1071 s]
```

![day05i](./figs/day05_start.png)

Starting with the batch based solution should "speed up" this at most by 16 (on my machine®). This would not be sufficient to the 100ms target, but 250ms without dealing with `async` would be nice. After implementing a batched calculation and spending some time "tunning" how many hashes should be pre-calculated, we reached a much smaller improvement. The best runtime was a little under 2.5s, and we see that must of the time is spent on rayon overhead. Benchmarking this solution also created the largest `perf.data` so far, 21G! Another aspect here is, since I was running day in parallel and this solution (and also day14) uses all the available core, we will have a total time close to day05 + day14 + largest of the other days. `cargo run --release` took 3.2s.

```
solution                time:   [2.4698 s 2.4789 s 2.4876 s]
                        change: [-38.900% -38.647% -38.410%] (p = 0.00 < 0.05)
```

![day05b](./figs/day05_batched.png)

With previous experience limited to embarrassingly parallel problems, some cursory looks over wikipedia, the rust book and the rust async book, and trying to get rid of all that time spent on locks, I've decided to check how I could use message passing. I have ended up with some multiple-producer single-consumer implementation that started with the example on the [rust book](https://doc.rust-lang.org/book/ch16-02-message-passing.html) later helped with this [blog](https://blog.softwaremill.com/multithreading-in-rust-with-mpsc-multi-producer-single-consumer-channels-db0fc91ae3fa) on "Multithreading in Rust with MPSC (Multi-Producer, Single Consumer) channels". The initial results were slower, back at the 4 seconds mark. 

```
solution                time:   [4.1089 s 4.1146 s 4.1210 s]
```

Trying to understand how to speed up my solution, I've picked up a few improvements [here](https://pastebin.com/hhBnnt3j) and [here](https://github.com/mmstick/advent_of_code_2016/blob/master/src/05/main.rs) that improved the runtime back to ~3s. Curiously, the second link have a commented line where a 3.5s runtime is mentioned.
```
solution                time:   [3.0349 s 3.0383 s 3.0419 s]
                        change: [-26.301% -26.157% -26.024%] (p = 0.00 < 0.05)
```

After digging more into it, I've realized that I was spawning only one thread, and due to the proportion of work between threads, the runtime ended up being the same as running it in a single thread. After rearranging the code and using some sharding over 10 workers, the runtime reduced to under 500ms.

```
solution                time:   [438.28 ms 443.44 ms 448.60 ms]
```

According to the flamegraph, most of the time is spent on `crypto::md5::Md5State::process_block`. With this, the total runtime is dominated by days 14 and 18 running close to 1.2s, followed by day05 that is probably lower limit for the runtime. With this, the idea of running everything under 100ms is probably unachievable without a custom md5 implemention (maybe there is a way to drop early from the calculation).

```
Day 14	1.21s
Day 18	1.16s
Day 05	552.20ms
```

## Day18

Initial baseline point to:
```
parse_input             time:   [3.6602 µs 3.6704 µs 3.6822 µs]
part 1                  time:   [45.698 µs 45.781 µs 45.874 µs]
part 2                  time:   [693.21 ms 695.61 ms 698.28 ms]
```
which is surprising, since the runtime was >1s. Since day14 uses rayon over all threads, maybe the timer for here is getting started but the solution is hanging while the machine is busy with day14. I knew that running all days in parallel and having a day using all cores was going to bite me back, and now the time has come. Just looking at the code for day18, I've realize that I was storing all the rows created, which is not necessary for this problem, as it just asks for the sum of tiles after N iterations. Since the initial `Vec` was already being allocated, this did not impact a lot the runtime, but yielded a 23% speedup:

```
part #2                 time:   [532.24 ms 532.62 ms 533.00 ms]
                        change: [-23.728% -23.431% -23.160%] (p = 0.00 < 0.05)
```

The flamegraph shows most of the time spent collecting on `create_new_row`:

```
    row.windows(3)
        .map(|v| {
            if (v == f1) || (v == f2) || (v == f3) || (v == f4) {
                1
            } else {
                0
            }
        })
        .collect()
}
```

Cloning the <Vec> before analysing the patterns and replacing the `.map` with `.for_each` while producing the output yielded another 43% improvement:
```
part #2                 time:   [302.51 ms 302.81 ms 303.13 ms]
                        change: [-43.218% -43.147% -43.077%] (p = 0.00 < 0.05)
```

With this improvement, we cracked the 1s barrier, achieving 859ms for all days with day 14 being the slowest and day 18 running faster than day05:

```
Day 14	861.29ms
Day 05	484.93ms
Day 18	442.78ms
```

## Day 14

While running day14 benchmark, we see again that the individual runtime is lower than what is shown in the output while running the whole year. This is probably a interaction between rayon on day14 and the rest of the days running in parallel (let's not forget also day05).
Day 14 shows 
```
solution                time:   [435.37 ms 436.43 ms 437.56 ms]
```
While the flamegraph presents most of the time spent in hashing and building up the results, I see no way to optimize this.
Considering that day18 takes close to 300ms, both days amount to more than 700ms, so we are probably near the limit of the performance without digging into md5.
